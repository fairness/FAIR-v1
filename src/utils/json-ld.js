import fields from "./fields";

export default {
  json2components: function(jsonld, options) {
    var components = [];

    // all dce:subjects in the same language are handled by 1 component
    let keywords = {};
    Object.entries(jsonld).forEach(([key, value]) => {
      //console.log("in here", key);
      if (key === "dce:subject") {
        for (let v of value) {
          if (v["@type"] === "skos:Concept") {
            for (let pl of v["skos:prefLabel"]) {
              let lang = pl["@language"] ? pl["@language"] : "xxx";
              if (!keywords[lang]) {
                keywords[lang] = [];
              }
              keywords[lang].push(pl["@value"]);
            }
          }
        }
      }
    });
    Object.entries(keywords).forEach(([key, value]) => {
      let f = fields.getField("keyword");
      if (key !== "xxx") {
        f.language = key;
      }
      f.value = value;
      components.push(f);
    });

    Object.entries(jsonld).forEach(([key, value]) => {
      if (key !== "@type") {
        var i;
        var j;
        for (i = 0; i < value.length; i++) {
          var f;
          //console.log("value", value[i]);
          switch (key) {
            // rdam:P30004
            case "rdam:P30004":
              f = fields.getField("alternate-identifier");
              f.type = value[i]["@type"];
              f.value = value[i]["@value"];
              components.push(f);
              break;

            // dcterms:type
            case "dcterms:type":
              //take this off ISA
              /* f = fields.getField("resource-type");
              for (let em of value[i]["skos:exactMatch"]) {
                f.value = em;
              }
              components.push(f);*/
              components.push({});
              break;

            // edm:hasType
            case "edm:hasType":
              f = fields.getField("object-type");
              for (let em of value[i]["skos:exactMatch"]) {
                f.value = em;
              }
              components.push(f);
              break;

            // schema:genre
            case "schema:genre":
              f = fields.getField("genre");
              for (let em of value[i]["skos:exactMatch"]) {
                f.value = em;
              }
              components.push(f);
              break;

            // oaire:version
            case "oaire:version":
              f = fields.getField("version-type");
              for (let em of value[i]["skos:exactMatch"]) {
                f.value = em;
              }
              components.push(f);
              break;

            // dcterms:accessRights
            case "dcterms:accessRights":
              f = fields.getField("access-right");
              for (let em of value[i]["skos:exactMatch"]) {
                f.value = em;
              }
              components.push(f);
              break;

            // dce:title
            case "dce:title":
              if (
                value[i]["@type"] === "bf:Title" ||
                value[i]["@type"] === "bf:ParallelTitle"
              ) {
                f = fields.getField("pharma-title");
                //console.log("value[i]['@type']", f);
                f.type = value[i]["@type"];
                if (value[i]["bf:mainTitle"]) {
                  for (j = 0; j < value[i]["bf:mainTitle"].length; j++) {
                    f.title = value[i]["bf:mainTitle"][j]["@value"];
                    f.language = value[i]["bf:mainTitle"][j]["@language"]
                      ? value[i]["bf:mainTitle"][j]["@language"]
                      : "eng";
                  }
                }
                if (value[i]["bf:subtitle"]) {
                  for (j = 0; j < value[i]["bf:subtitle"].length; j++) {
                    f.subtitle = value[i]["bf:subtitle"][j]["@value"];
                  }
                }
                components.push(f);
              }
              break;

            // bf:note
            case "bf:note":
              switch (value[i]["@type"]) {
                case "bf:Note":
                  f = fields.getField("description");
                  break;
                case "bf:Summary":
                  f = fields.getField("abstract");
                  break;
                case "phaidra:Remark":
                  f = fields.getField("note");
                  break;
                case "phaidra:DigitizationNote":
                  f = fields.getField("digitization-note");
                  break;
                case "arm:ConditionAssessment":
                  f = fields.getField("condition-note");
                  break;
                case "phaidra:ReproductionNote":
                  f = fields.getField("reproduction-note");
                  break;
              }

              for (let prefLabel of value[i]["skos:prefLabel"]) {
                f.value = prefLabel["@value"];
                f.language = prefLabel["@language"]
                  ? prefLabel["@language"]
                  : "";
              }
              components.push(f);
              break;

            // bf:tableOfContents
            case "bf:tableOfContents":
              f = fields.getField("table-of-contents");
              for (let prefLabel of value[i]["skos:prefLabel"]) {
                f.value = prefLabel["@value"];
                f.language = prefLabel["@language"]
                  ? prefLabel["@language"]
                  : "";
              }
              components.push(f);
              break;

            // dcterms:language
            case "dcterms:language":
              f = fields.getField("language");
              for (j = 0; j < value[i].length; j++) {
                f.value = value[i];
              }
              components.push(f);
              break;

            // schema:subtitleLanguage
            case "schema:subtitleLanguage":
              f = fields.getField("subtitle-language");
              for (j = 0; j < value[i].length; j++) {
                f.value = value[i];
              }
              components.push(f);
              break;

            // dce:subject
            case "dce:subject":
              // noop - we handled this already
              break;

            // dcterms:subject
            case "dcterms:subject":
              if (value[i]["@type"] === "skos:Concept") {
                //ISA THAT THIS OFF FOR NOW
                /* f = fields.getField("vocab-ext-readonly");
                f["skos:exactMatch"] = value[i]["skos:exactMatch"];
                f["skos:prefLabel"] = value[i]["skos:prefLabel"];
                f["rdfs:label"] = value[i]["rdfs:label"];
                if (value[i]["skos:exactMatch"]) {
                  for (j = 0; j < value[i]["skos:exactMatch"].length; j++) {
                    f.value = value[i]["skos:exactMatch"][j];
                  }
                }
                if (value[i]["skos:notation"]) {
                  f["skos:notation"] = [];
                  for (j = 0; j < value[i]["skos:notation"].length; j++) {
                    f["skos:notation"].push(value[i]["skos:notation"][j]);
                  }
                }
                f.predicate = key;
                f.label = "Classification";
                components.push(f);*/
                components.push({});
                // TODO: add classification
              } else if (value[i]["@type"] === "phaidra:Subject") {
                // ignore, handled elsewhere
              } else {
              }
              break;

            case "rdau:P60193":
              f = fields.getField("series");
              if (value[i]["dce:title"]) {
                for (let t of value[i]["dce:title"]) {
                  if (t["bf:mainTitle"]) {
                    for (let mt of t["bf:mainTitle"]) {
                      if (mt["@value"]) {
                        f.title = mt["@value"];
                      }
                      if (mt["@language"]) {
                        f.titleLanguage = mt["@language"];
                      }
                    }
                  }
                }
              }
              if (value[i]["bibo:volume"]) {
                for (let v of value[i]["bibo:volume"]) {
                  f.volume = v;
                }
              }
              if (value[i]["bibo:issue"]) {
                for (let v of value[i]["bibo:issue"]) {
                  f.issue = v;
                }
              }
              if (value[i]["dcterms:issued"]) {
                for (let v of value[i]["dcterms:issued"]) {
                  f.issued = v;
                }
              }
              if (value[i]["identifiers:issn"]) {
                for (let v of value[i]["identifiers:issn"]) {
                  f.issn = v;
                }
              }
              if (value[i]["skos:exactMatch"]) {
                for (let v of value[i]["skos:exactMatch"]) {
                  f.identifier = v;
                }
              }
              Object.entries(jsonld).forEach(([key1, value1]) => {
                if (key1 === "schema:pageStart") {
                  for (let ps of value1) {
                    f.pageStart = ps;
                  }
                }
                if (key1 === "schema:pageEnd") {
                  for (let pe of value1) {
                    f.pageEnd = pe;
                  }
                }
              });
              components.push(f);
              break;

            case "rdau:P60101":
              f = fields.getField("contained-in");
              if (value[i]["dce:title"]) {
                for (let t of value[i]["dce:title"]) {
                  if (t["bf:mainTitle"]) {
                    for (let mt of t["bf:mainTitle"]) {
                      if (mt["@value"]) {
                        f.title = mt["@value"];
                      }
                      if (mt["@language"]) {
                        f.titleLanguage = mt["@language"];
                      }
                    }
                  }
                  if (t["bf:subtitle"]) {
                    for (let st of t["bf:subtitle"]) {
                      if (st["@value"]) {
                        f.subtitle = st["@value"];
                      }
                    }
                  }
                }
              }
              f.roles = [];
              Object.entries(value[i]).forEach(([key, value]) => {
                if (key.startsWith("role")) {
                  let roleidx = 0;
                  for (let role of value) {
                    roleidx++;
                    let entity = {
                      id: "contained-in-role-" + roleidx,
                      role: key,
                      ordergroup: "contained-in-role",
                    };
                    if (role["schema:name"]) {
                      for (let name of role["schema:name"]) {
                        entity.name = name["@value"];
                      }
                    }
                    if (role["schema:familyName"]) {
                      for (let lastname of role["schema:familyName"]) {
                        entity.lastname = lastname["@value"];
                      }
                    }
                    if (role["schema:givenName"]) {
                      for (let firstname of role["schema:givenName"]) {
                        entity.firstname = firstname["@value"];
                      }
                    }
                    f.roles.push(entity);
                  }
                }
              });
              Object.entries(jsonld).forEach(([key1, value1]) => {
                if (key1 === "schema:pageStart") {
                  for (let ps of value1) {
                    f.pageStart = ps;
                  }
                }
                if (key1 === "schema:pageEnd") {
                  for (let pe of value1) {
                    f.pageEnd = pe;
                  }
                }
              });
              if (value[i]["rdau:P60193"]) {
                for (let series of value[i]["rdau:P60193"]) {
                  if (series["dce:title"]) {
                    for (let t of series["dce:title"]) {
                      if (t["bf:mainTitle"]) {
                        for (let mt of t["bf:mainTitle"]) {
                          if (mt["@value"]) {
                            f.seriesTitle = mt["@value"];
                          }
                          if (mt["@language"]) {
                            f.seriesTitleLanguage = mt["@language"];
                          }
                        }
                      }
                    }
                  }
                  if (series["bibo:volume"]) {
                    for (let v of series["bibo:volume"]) {
                      f.seriesVolume = v;
                    }
                  }
                  if (series["bibo:issue"]) {
                    for (let v of series["bibo:issue"]) {
                      f.seriesIssue = v;
                    }
                  }
                  if (series["dcterms:issued"]) {
                    for (let v of series["dcterms:issued"]) {
                      f.seriesIssued = v;
                    }
                  }
                  if (series["identifiers:issn"]) {
                    for (let v of series["identifiers:issn"]) {
                      f.seriesIssn = v;
                    }
                  }
                  if (series["skos:exactMatch"]) {
                    for (let v of series["skos:exactMatch"]) {
                      f.seriesIdentifier = v;
                    }
                  }
                }
              }
              components.push(f);
              break;

            // dcterms:temporal
            case "dcterms:temporal":
              f = fields.getField("temporal-coverage");
              f.value = value[i]["@value"];
              f.language = value[i]["@language"]
                ? value[i]["@language"]
                : "eng";
              components.push(f);
              break;

            // spatial
            case "dcterms:spatial":
            case "vra:placeOfCreation":
            case "vra:placeOfRepository":
            case "vra:placeOfSite":
              if (
                value[i]["@type"] === "schema:Place" &&
                !value[i]["skos:exactMatch"]
              ) {
                // freetext
                f = fields.getField("spatial-text");
                f.type = key;
                for (j = 0; j < value[i]["skos:prefLabel"].length; j++) {
                  f.value = value[i]["skos:prefLabel"][j]["@value"];
                  f.language = value[i]["skos:prefLabel"][j]["@language"]
                    ? value[i]["skos:prefLabel"][j]["@language"]
                    : "eng";
                }
                components.push(f);
              } else {
                if (
                  value[i]["@type"] === "schema:Place" &&
                  value[i]["skos:exactMatch"]
                ) {
                  // getty
                  f = fields.getField("spatial-getty-readonly");
                  if (value[i]["skos:exactMatch"]) {
                    for (j = 0; j < value[i]["skos:exactMatch"].length; j++) {
                      f.value = value[i]["skos:exactMatch"][j];
                    }
                  }
                  f["skos:prefLabel"] = value[i]["skos:prefLabel"];
                  f["rdfs:label"] = value[i]["rdfs:label"];
                  f.coordinates = value[i]["schema:geo"];
                  f.predicate = key;
                  f.type = key;
                  f.label = key;
                  components.push(f);
                  f = fields.getField("spatial-getty");
                  f.predicate = key;
                  f.type = key;
                  components.push(f);
                }
              }
              break;

            // dce:format
            case "dce:format":
              f = fields.getField("dce-format-vocab");
              for (let em of value[i]["skos:exactMatch"]) {
                f.value = em;
              }
              components.push(f);
              break;

            // rdau:P60048
            case "rdau:P60048":
              f = fields.getField("carrier-type");
              for (let em of value[i]["skos:exactMatch"]) {
                f.value = em;
              }
              components.push(f);
              break;

            // edm:rights
            case "edm:rights":
              f = fields.getField("license");
              f.vocabulary = "alllicenses";
              f.value = value[i];
              components.push(f);
              break;

            // dce:rights
            case "dce:rights":
              f = fields.getField("rights");
              f.value = value[i]["@value"];
              f.language = value[i]["@language"]
                ? value[i]["@language"]
                : "eng";
              components.push(f);
              break;

            // bf:provisionActivity
            case "bf:provisionActivity":
              f = fields.getField("bf-publication");
              if (value[i]["bf:agent"]) {
                for (let pub of value[i]["bf:agent"]) {
                  if (pub["skos:exactMatch"]) {
                    for (let id of pub["skos:exactMatch"]) {
                      if (id.startsWith("https://pid.phaidra.org/")) {
                        f.publisherType = "select";
                        f.publisherOrgUnit = id;
                      } else {
                        f.publisherType = "other";
                        if (pub["schema:name"]) {
                          for (let name of pub["schema:name"]) {
                            f.publisherName = name["@value"];
                          }
                        }
                      }
                    }
                  } else {
                    if (pub["schema:name"]) {
                      f.publisherType = "other";
                      if (pub["schema:name"]) {
                        for (let name of pub["schema:name"]) {
                          f.publisherName = name["@value"];
                        }
                      }
                    }
                  }
                }
              }
              if (value[i]["bf:place"]) {
                for (let pl of value[i]["bf:place"]) {
                  if (pl["skos:prefLabel"]) {
                    for (let pllab of pl["skos:prefLabel"]) {
                      f.publishingPlace = pllab["@value"];
                    }
                  }
                }
              }
              if (value[i]["bf:date"]) {
                for (let pdate of value[i]["bf:date"]) {
                  f.publishingDate = pdate;
                }
              }
              components.push(f);
              break;

            // citation
            case "cito:cites":
            case "cito:isCitedBy":
              f = fields.getField("citation");
              f.type = key;
              for (let prefLabel of value[i]["skos:prefLabel"]) {
                f.citation = prefLabel["@value"];
                f.citationLanguage = prefLabel["@language"]
                  ? prefLabel["@language"]
                  : "";
              }
              for (let em of value[i]["skos:exactMatch"]) {
                f.identifier = em;
              }
              components.push(f);
              break;

            // rdau:P60227
            case "rdau:P60227":
              f = fields.getField("movieadaptation");
              if (value[i]["dce:title"]) {
                for (let t of value[i]["dce:title"]) {
                  if (t["bf:mainTitle"]) {
                    for (let mt of t["bf:mainTitle"]) {
                      if (mt["@value"]) {
                        f.title = mt["@value"];
                      }
                      if (mt["@language"]) {
                        f.titleLanguage = mt["@language"];
                      }
                    }
                    if (t["bf:subtitle"]) {
                      for (let st of t["bf:subtitle"]) {
                        if (st["@value"]) {
                          f.subtitle = st["@value"];
                        }
                      }
                    }
                  }
                }
              }
              for (let [pred, obj] of Object.entries(value[i])) {
                if (pred.startsWith("role")) {
                  for (let role of obj) {
                    if (role["@type"] === "schema:Person") {
                      f.role = pred;
                      if (role["schema:name"]) {
                        for (let name of role["schema:name"]) {
                          f.name = name["@value"];
                          f.showname = true;
                        }
                      }
                      if (role["schema:familyName"]) {
                        for (let lastname of role["schema:familyName"]) {
                          f.lastname = lastname["@value"];
                        }
                      }
                      if (role["schema:givenName"]) {
                        for (let firstname of role["schema:givenName"]) {
                          f.firstname = firstname["@value"];
                        }
                      }
                    }
                  }
                }
              }
              components.push(f);
              break;

            // frapo:hasFundingAgency
            case "frapo:hasFundingAgency":
              if (value[i]["@type"] === "frapo:FundingAgency") {
                //ISA take here off
                /* f = fields.getField("funder");
                if (value[i]["skos:prefLabel"]) {
                  for (j = 0; j < value[i]["skos:prefLabel"].length; j++) {
                    f.name = value[i]["skos:prefLabel"][j]["@value"];
                    f.nameLanguage = value[i]["skos:prefLabel"][j]["@language"]
                      ? value[i]["skos:prefLabel"][j]["@language"]
                      : "eng";
                  }
                }
                if (value[i]["skos:exactMatch"]) {
                  for (let em of value[i]["skos:exactMatch"]) {
                    f.identifier = em;
                  }
                }
                components.push(f);*/
                components.push({});
              }
              break;

            // frapo:isOutputOf
            case "frapo:isOutputOf":
              if (value[i]["@type"] === "foaf:Project") {
                //ISA take here off
                /* f = fields.getField("project");
                if (value[i]["skos:prefLabel"]) {
                  for (j = 0; j < value[i]["skos:prefLabel"].length; j++) {
                    f.name = value[i]["skos:prefLabel"][j]["@value"];
                    f.nameLanguage = value[i]["skos:prefLabel"][j]["@language"]
                      ? value[i]["skos:prefLabel"][j]["@language"]
                      : "eng";
                  }
                }
                if (value[i]["rdfs:comment"]) {
                  for (j = 0; j < value[i]["rdfs:comment"].length; j++) {
                    f.description = value[i]["rdfs:comment"][j]["@value"];
                    f.descriptionLanguage = value[i]["rdfs:comment"][j][
                      "@language"
                    ]
                      ? value[i]["rdfs:comment"][j]["@language"]
                      : "eng";
                  }
                }
                if (value[i]["skos:exactMatch"]) {
                  for (j = 0; j < value[i]["skos:exactMatch"].length; j++) {
                    f.identifier = value[i]["skos:exactMatch"][j];
                  }
                }
                if (value[i]["foaf:homepage"]) {
                  for (j = 0; j < value[i]["foaf:homepage"].length; j++) {
                    f.homepage = value[i]["foaf:homepage"][j];
                  }
                }
                if (value[i]["frapo:hasFundingAgency"]) {
                  for (let funder of value[i]["frapo:hasFundingAgency"]) {
                    if (funder["skos:prefLabel"]) {
                      for (let pl of funder["skos:prefLabel"]) {
                        f.funderName = pl["@value"];
                        f.funderNameLanguage = pl["@language"]
                          ? pl["@language"]
                          : "eng";
                      }
                    }
                    if (funder["skos:exactMatch"]) {
                      for (let em of funder["skos:exactMatch"]) {
                        f.funderIdentifier = em;
                      }
                    }
                  }
                }
                components.push(f);*/
                components.push({});
              } else {
                if (value[i]["@type"] === "aaiso:Programme") {
                  f = fields.getField("study-plan");
                  if (value[i]["skos:prefLabel"]) {
                    for (j = 0; j < value[i]["skos:prefLabel"].length; j++) {
                      f.name = value[i]["skos:prefLabel"][j]["@value"];
                      f.nameLanguage = value[i]["skos:prefLabel"][j][
                        "@language"
                      ]
                        ? value[i]["skos:prefLabel"][j]["@language"]
                        : "eng";
                    }
                  }
                  if (value[i]["skos:notation"]) {
                    for (j = 0; j < value[i]["skos:notation"].length; j++) {
                      f.notation = value[i]["skos:notation"][j];
                    }
                  }
                  components.push(f);
                }
              }
              break;

            // rdax:P00009
            case "rdax:P00009":
              f = fields.getField("association");
              for (let em of value[i]["skos:exactMatch"]) {
                f.value = em;
              }
              components.push(f);
              break;

            // dcterms:provenance
            case "dcterms:provenance":
              if (value[i]["@type"] === "dcterms:ProvenanceStatement") {
                f = fields.getField("provenance");
                for (j = 0; j < value[i]["skos:prefLabel"].length; j++) {
                  f.value = value[i]["skos:prefLabel"][j]["@value"];
                  f.language = value[i]["skos:prefLabel"][j]["@language"]
                    ? value[i]["skos:prefLabel"][j]["@language"]
                    : "eng";
                }
                components.push(f);
              }
              break;

            // schema:numberOfPages
            case "schema:numberOfPages":
              f = fields.getField("number-of-pages");
              f.label = key;
              f.value = value[i];
              components.push(f);
              break;

            // bf:soundCharacteristic
            case "bf:soundCharacteristic":
              f = fields.getField("sound-characteristic");
              f.label = key;
              f.value = value[i];
              components.push(f);
              break;

            // bf:supplementaryContent
            case "bf:supplementaryContent":
              f = fields.getField("supplementary-content");
              for (j = 0; j < value[i]["skos:prefLabel"].length; j++) {
                f.value = value[i]["skos:prefLabel"][j]["@value"];
                f.language = value[i]["skos:prefLabel"][j]["@language"]
                  ? value[i]["skos:prefLabel"][j]["@language"]
                  : "eng";
              }
              components.push(f);
              break;

            // bf:awards
            case "bf:awards":
              f = fields.getField("award");
              for (j = 0; j < value[i]["skos:prefLabel"].length; j++) {
                f.value = value[i]["skos:prefLabel"][j]["@value"];
                f.language = value[i]["skos:prefLabel"][j]["@language"]
                  ? value[i]["skos:prefLabel"][j]["@language"]
                  : "eng";
              }
              components.push(f);
              break;

            // rdau:P60059
            case "rdau:P60059":
              f = fields.getField("regional-encoding");
              for (let em of value[i]["skos:exactMatch"]) {
                f.value = em;
              }
              components.push(f);
              break;

            // ebucore:filename
            case "ebucore:filename":
              f = fields.getField("filename");
              f.predicate = key;
              f.label = key;
              f.value = value[i];
              components.push(f);
              break;

            // ebucore:hasMimeType
            case "ebucore:hasMimeType":
              f = fields.getField("mime-type");
              for (j = 0; j < value[i].length; j++) {
                f.value = value[i];
              }
              components.push(f);
              break;

            // opaque:cco_accessionNumber
            case "opaque:cco_accessionNumber":
              f = fields.getField("accession-number");
              for (j = 0; j < value[i].length; j++) {
                f.value = value[i];
              }
              components.push(f);
              break;

            // bf:shelfMark
            case "bf:shelfMark":
              f = fields.getField("shelf-mark");
              for (j = 0; j < value[i].length; j++) {
                f.value = value[i];
              }
              components.push(f);
              break;

            // bf:physicalLocation
            case "bf:physicalLocation":
              f = fields.getField("physical-location");
              f.value = value[i]["@value"];
              f.language = value[i]["@language"]
                ? value[i]["@language"]
                : "eng";
              components.push(f);
              break;

            // rdau:P60550
            case "rdau:P60550":
              f = fields.getField("extent");
              f.value = value[i]["@value"];
              f.language = value[i]["@language"]
                ? value[i]["@language"]
                : "eng";
              components.push(f);
              break;

            // vra:hasInscription
            case "vra:hasInscription":
              if (value[i]["@type"] === "vra:Inscription") {
                f = fields.getField("inscription");
                for (j = 0; j < value[i]["skos:prefLabel"].length; j++) {
                  f.value = value[i]["skos:prefLabel"][j]["@value"];
                  f.language = value[i]["skos:prefLabel"][j]["@language"]
                    ? value[i]["skos:prefLabel"][j]["@language"]
                    : "eng";
                }
                components.push(f);
              }
              break;

            // bf:scale
            case "bf:scale":
              if (value[i]["@type"] === "bf:Scale") {
                f = fields.getField("scale");
                for (j = 0; j < value[i]["skos:prefLabel"].length; j++) {
                  f.value = value[i]["skos:prefLabel"][j]["@value"];
                }
                components.push(f);
              }
              break;

            // vra:material
            case "vra:material":
              if (value[i]["@type"] === "vra:Material") {
                f = fields.getField("material-text");
                for (j = 0; j < value[i]["skos:prefLabel"].length; j++) {
                  f.value = value[i]["skos:prefLabel"][j]["@value"];
                  f.language = value[i]["skos:prefLabel"][j]["@language"]
                    ? value[i]["skos:prefLabel"][j]["@language"]
                    : "eng";
                }
                components.push(f);
              } else {
                // vra:material - vocab
                if (
                  value[i]["@type"] === "vra:material" &&
                  value[i]["skos:exactMatch"]
                ) {
                  f = fields.getField("material-vocab");
                  for (let em of value[i]["skos:exactMatch"]) {
                    f.value = em;
                  }
                  components.push(f);
                }
              }
              break;

            // vra:hasTechnique
            case "vra:hasTechnique":
              if (
                value[i]["@type"] === "vra:Technique" &&
                !value[i]["skos:exactMatch"]
              ) {
                f = fields.getField("technique-text");
                for (j = 0; j < value[i]["skos:prefLabel"].length; j++) {
                  f.value = value[i]["skos:prefLabel"][j]["@value"];
                  f.language = value[i]["skos:prefLabel"][j]["@language"]
                    ? value[i]["skos:prefLabel"][j]["@language"]
                    : "eng";
                }
                components.push(f);
              } else {
                // vra:hasTechnique - vocab
                if (
                  value[i]["@type"] === "vra:Technique" &&
                  value[i]["skos:exactMatch"]
                ) {
                  f = fields.getField("technique-vocab");
                  for (let em of value[i]["skos:exactMatch"]) {
                    f.value = em;
                  }
                  components.push(f);
                }
              }
              break;

            // dcterms:audience
            case "dcterms:audience":
              if (
                value[i]["@type"] === "dcterms:audience" &&
                !value[i]["skos:exactMatch"]
              ) {
                f = fields.getField("audience");
                for (j = 0; j < value[i]["skos:prefLabel"].length; j++) {
                  f.value = value[i]["skos:prefLabel"][j]["@value"];
                  f.language = value[i]["skos:prefLabel"][j]["@language"]
                    ? value[i]["skos:prefLabel"][j]["@language"]
                    : "eng";
                }
                components.push(f);
              } else {
                // dcterms:audience - select
                if (
                  value[i]["@type"] === "dcterms:audience" &&
                  value[i]["skos:exactMatch"]
                ) {
                  f = fields.getField("audience-vocab");
                  for (let em of value[i]["skos:exactMatch"]) {
                    f.value = em;
                  }
                  components.push(f);
                }
              }
              break;

            // schema:width
            case "schema:width":
              if (value[i]["@type"] === "schema:QuantitativeValue") {
                f = fields.getField("width");
                for (j = 0; j < value[i]["schema:unitCode"].length; j++) {
                  f.unit = value[i]["schema:unitCode"][j];
                }
                for (j = 0; j < value[i]["schema:value"].length; j++) {
                  f.value = value[i]["schema:value"][j];
                }
                components.push(f);
              }
              break;

            // schema:height
            case "schema:height":
              if (value[i]["@type"] === "schema:QuantitativeValue") {
                f = fields.getField("height");
                for (j = 0; j < value[i]["schema:unitCode"].length; j++) {
                  f.unit = value[i]["schema:unitCode"][j];
                }
                for (j = 0; j < value[i]["schema:value"].length; j++) {
                  f.value = value[i]["schema:value"][j];
                }
                components.push(f);
              }
              break;

            // schema:depth
            case "schema:depth":
              if (value[i]["@type"] === "schema:QuantitativeValue") {
                f = fields.getField("depth");
                for (j = 0; j < value[i]["schema:unitCode"].length; j++) {
                  f.unit = value[i]["schema:unitCode"][j];
                }
                for (j = 0; j < value[i]["schema:value"].length; j++) {
                  f.value = value[i]["schema:value"][j];
                }
                components.push(f);
              }
              break;

            // dates
            case "dcterms:date":
            case "dcterms:created":
            case "dcterms:modified":
            case "dcterms:available":
            case "dcterms:issued":
            case "dcterms:valid":
            case "dcterms:dateAccepted":
            case "dcterms:dateCopyrighted":
            case "dcterms:dateSubmitted":
            case "rdau:P60071":
            case "phaidra:dateAccessioned":
              // only edtf now (later time can be edm:TimeSpan)
              //COMMENT ISA THIS IS NOT WORKING< COMMENT OUT
              /* if (typeof value[i] === "string") {
                f = fields.getField("date-edtf");
                f.type = key;
                f.value = value[i];
                components.push(f);
              }*/
              components.push({});
              break;

            // schema:duration
            case "schema:duration":
              f = fields.getField("duration");
              for (j = 0; j < value[i].length; j++) {
                f.value = value[i];
              }
              components.push(f);
              break;

            // phaidra:systemTag
            case "phaidra:systemTag":
              f = fields.getField("system-tag");
              for (j = 0; j < value[i].length; j++) {
                f.value = value[i];
              }
              components.push(f);
              break;

            // pages, handled insisde rdau:P60193 or rdau:P60101
            case "schema:pageStart":
            case "schema:pageEnd":
              // noop
              break;

            default:
              // role
              //ISA off
              /*if (key.startsWith("role")) {
                let role = value[i];
                if (
                  options &&
                  options["role"] &&
                  options["role"]["component"] &&
                  options["role"]["component"] === "p-entity"
                ) {
                  f = fields.getField("role");
                } else {
                  f = fields.getField("role-extended");
                }
                f.role = key;
                f.type = role["@type"];
                if (role["@type"] === "schema:Person") {
                  if (role["schema:name"]) {
                    for (let name of role["schema:name"]) {
                      f.name = name["@value"];
                      f.showname = true;
                    }
                  }
                  if (role["schema:familyName"]) {
                    for (let lastname of role["schema:familyName"]) {
                      f.lastname = lastname["@value"];
                    }
                  }
                  if (role["schema:givenName"]) {
                    for (let firstname of role["schema:givenName"]) {
                      f.firstname = firstname["@value"];
                    }
                  }
                  if (role["skos:exactMatch"]) {
                    for (let id of role["skos:exactMatch"]) {
                      f.identifierType = id["@type"];
                      f.identifierText = id["@value"];
                    }
                  }
                  if (role["schema:affiliation"]) {
                    for (let af of role["schema:affiliation"]) {
                      if (af["skos:exactMatch"]) {
                        for (let id of af["skos:exactMatch"]) {
                          if (id.startsWith("https://pid.phaidra.org/")) {
                            f.affiliationType = "select";
                            f.affiliation = id;
                          } else {
                            f.affiliationType = "other";
                            if (af["schema:name"]) {
                              for (let name of af["schema:name"]) {
                                f.affiliationText = name["@value"];
                              }
                            }
                          }
                        }
                      } else {
                        if (af["schema:name"]) {
                          f.affiliationType = "other";
                          for (let name of af["schema:name"]) {
                            f.affiliationText = name["@value"];
                          }
                        }
                      }
                    }
                  }
                }
                if (role["@type"] === "schema:Organization") {
                  if (role["skos:exactMatch"]) {
                    for (let id of role["skos:exactMatch"]) {
                      if (id.startsWith("https://pid.phaidra.org/")) {
                        f.organizationType = "select";
                        f.organization = id;
                      } else {
                        f.organizationType = "other";
                        f.identifierText = id;
                        if (role["schema:name"]) {
                          for (let name of role["schema:name"]) {
                            f.organizationText = name["@value"];
                          }
                        }
                      }
                    }
                  } else {
                    if (role["schema:name"]) {
                      for (let name of role["schema:name"]) {
                        f.organizationText = name["@value"];
                      }
                    }
                  }
                }
                components.push(f);
              } else {
                // unknown predicate
                f = fields.getField("readonly");
                f.jsonld = value[i];
                f.predicate = key;
                f.label = key;
                components.push(f);
              }*/
              components.push({});
              break;
          }
        }
      }
    });

    return components;
  },
  getOrderedComponents: function(components) {
    var predicateOrder = fields.getPredicateOrder();
    var ordered = [];
    var i;
    var j;
    for (i = 0; i < predicateOrder.length; i++) {
      for (j = 0; j < components.length; j++) {
        if (components[j].predicate === predicateOrder[i]) {
          ordered.push(components[j]);
        }
      }
    }
    for (j = 0; j < components.length; j++) {
      if (components[j].component === "p-unknown-readonly") {
        ordered.push(components[j]);
      }
    }
    return ordered;
  },
  json2form: function(jsonld, options) {
    var levels = {
      digital: {
        components: [],
      },
    };

    levels.digital.components = this.json2components(jsonld, options);

    Object.entries(jsonld).forEach(([key, value]) => {
      var i;

      if (key === "dcterms:subject") {
        levels["subject"] = [];
        for (i = 0; i < value.length; i++) {
          if (value[i]["@type"] === "phaidra:Subject") {
            var subcomp = this.json2components(value[i], options);
            if (subcomp.length > 0) {
              levels.subject.push({
                components: subcomp,
              });
            }
          }
        }
      }
    });

    var form = {
      sections: [],
    };
    console.log(
      "this.getOrderedComponents(levels.digital.components)",
      levels.digital.components
    );
    var digitalFields = this.getOrderedComponents(levels.digital.components);

    form.sections.push({
      title: "General metadata",
      id: 1,
      fields: digitalFields,
    });

    if (levels["subject"]) {
      for (var i = 0; i < levels.subject.length; i++) {
        var subjectFields = this.getOrderedComponents(
          levels.subject[i].components
        );
        form.sections.push({
          title: "Subject",
          type: "phaidra:Subject",
          id: "subject-" + i,
          fields: subjectFields,
        });
      }
    }

    return form;
  },

  get_json_dce_title(type, title, subtitle, language) {
    var h = {
      "@type": type,
      "bf:mainTitle": [
        {
          "@value": title,
        },
      ],
    };
    if (language) {
      h["bf:mainTitle"][0]["@language"] = language;
    }
    if (subtitle) {
      h["bf:subtitle"] = [
        {
          "@value": subtitle,
        },
      ];
      if (language) {
        h["bf:subtitle"][0]["@language"] = language;
      }
    }
    return h;
  },

  get_json_object(preflabels, rdfslabels, type, identifiers) {
    var h = {};

    if (type) {
      h["@type"] = type;
    }

    if (preflabels) {
      if (preflabels.length > 0) {
        h["skos:prefLabel"] = [];
        for (var j = 0; j < preflabels.length; j++) {
          h["skos:prefLabel"].push(preflabels[j]);
        }
      }
    }

    if (rdfslabels) {
      if (rdfslabels.length > 0) {
        h["rdfs:label"] = [];
        for (var i = 0; i < rdfslabels.length; i++) {
          h["rdfs:label"].push(rdfslabels[i]);
        }
      }
    }

    if (identifiers) {
      if (identifiers.length > 0) {
        h["skos:exactMatch"] = identifiers;
      }
    }
    return h;
  },
  get_json_concept(preflabels, rdfslabels, type, identifiers, notations) {
    var h = {};

    if (type) {
      h["@type"] = type;
    }

    if (preflabels) {
      if (preflabels.length > 0) {
        h["skos:prefLabel"] = [];
        for (var j = 0; j < preflabels.length; j++) {
          h["skos:prefLabel"].push({
            "@value": preflabels[j],
          });
        }
      }
    }

    if (rdfslabels) {
      if (rdfslabels.length > 0) {
        h["rdfs:label"] = [];
        for (var i = 0; i < rdfslabels.length; i++) {
          h["rdfs:label"].push(rdfslabels[i]);
        }
      }
    }

    if (identifiers) {
      if (identifiers.length > 0) {
        h["skos:exactMatch"] = identifiers;
      }
    }

    if (notations) {
      if (notations.length > 0) {
        h["skos:notation"] = notations;
      }
    }
    return h;
  },
  get_json_spatial(rdfslabels, preflabels, coordinates, type, identifiers) {
    var h = {};

    if (type) {
      h["@type"] = type;
    }

    if (preflabels) {
      if (preflabels.length > 0) {
        h["skos:prefLabel"] = [];
        for (var i = 0; i < preflabels.length; i++) {
          h["skos:prefLabel"].push(preflabels[i]);
        }
      }
    }

    if (rdfslabels) {
      if (rdfslabels.length > 0) {
        h["rdfs:label"] = [];
        for (var j = 0; j < rdfslabels.length; j++) {
          h["rdfs:label"].push(rdfslabels[j]);
        }
      }
    }

    if (identifiers) {
      if (identifiers.length > 0) {
        h["skos:exactMatch"] = identifiers;
      }
    }

    if (coordinates) {
      if (coordinates.length > 0) {
        h["schema:geo"] = coordinates;
      }
    }

    return h;
  },
  get_json_valueobject(value, language) {
    var h = {
      "@value": value,
    };

    if (language) {
      h["@language"] = language;
    }

    return h;
  },
  get_json_quantitativevalue(value, unitCode) {
    var h = {
      "@type": "schema:QuantitativeValue",
      "schema:unitCode": [unitCode],
      "schema:value": [value],
    };

    return h;
  },
  get_json_role(type, firstname, lastname, institution, email, orcid) {
    var h = {
      "@type": type,
    };
    if (firstname) {
      h["schema:givenName"] = [
        {
          "@value": firstname,
        },
      ];
    }
    if (lastname) {
      h["schema:familyName"] = [
        {
          "@value": lastname,
        },
      ];
    }
    if (institution) {
      h["schema:affiliation"] = [];
      for (var i = 0; i < institution.length; i++) {
        h["schema:affiliation"].push(institution[i]);
      }
    }
    if (email) {
      h["schema:email"] = [
        {
          "@value": email,
        },
      ];
    }
    if (orcid) {
      h["skos:exactMatch"] = orcid;
    }

    return h;
  },
  get_json_general_description(description, descriptionLanguage) {
    var h = {
      "@type": "bf:Note",
    };

    if (description) {
      h["skos:prefLabel"] = [
        {
          "@value": description,
        },
      ];
      if (descriptionLanguage) {
        h["skos:prefLabel"][0]["@language"] = descriptionLanguage;
      }
      return h;
    }
  },
  get_json_abstract(description, descriptionLanguage) {
    var h = {
      "@type": "bf:Summary",
    };

    if (description) {
      h["skos:prefLabel"] = [
        {
          "@value": description,
        },
      ];
      if (descriptionLanguage) {
        h["skos:prefLabel"][0]["@language"] = descriptionLanguage;
      }
      return h;
    }
  },
  get_json_project(
    name,
    nameLanguage,
    description,
    descriptionLanguage,
    identifiers,
    homepage
  ) {
    var h = {
      "@type": "foaf:Project",
    };

    if (name) {
      h["skos:prefLabel"] = [
        {
          "@value": name,
        },
      ];
      if (nameLanguage) {
        h["skos:prefLabel"][0]["@language"] = nameLanguage;
      }
    }
    if (description) {
      h["rdfs:comment"] = [
        {
          "@value": description,
        },
      ];
      if (descriptionLanguage) {
        h["rdfs:comment"][0]["@language"] = descriptionLanguage;
      }
    }
    if (identifiers) {
      if (identifiers.length > 0) {
        h["skos:exactMatch"] = identifiers;
      }
    }
    if (homepage) {
      h["foaf:homepage"] = [homepage];
    }
    return h;
  },
  get_json_series(
    type,
    title,
    titleLanguage,
    volume,
    issue,
    issued,
    issn,
    identifiers
  ) {
    var h = {
      "@type": type,
    };
    if (title) {
      let tit = {
        "@type": "bf:Title",
        "bf:mainTitle": [
          {
            "@value": title,
          },
        ],
      };
      if (titleLanguage) {
        tit["bf:mainTitle"][0]["@language"] = titleLanguage;
      }
      h["dce:title"] = [tit];
    }
    if (volume) {
      h["bibo:volume"] = [volume];
    }
    if (issue) {
      h["bibo:issue"] = [issue];
    }
    if (issued) {
      h["dcterms:issued"] = [issued];
    }
    if (issn) {
      h["identifiers:issn"] = [issn];
    }
    if (identifiers) {
      if (identifiers.length > 0) {
        h["skos:exactMatch"] = identifiers;
      }
    }
    return h;
  },
  get_json_adaptation(
    type,
    title,
    subtitle,
    titleLanguage,
    role,
    name,
    firstname,
    lastname
  ) {
    var h = {
      "@type": type,
    };
    if (title) {
      let tit = {
        "@type": "bf:Title",
        "bf:mainTitle": [
          {
            "@value": title,
          },
        ],
      };
      if (titleLanguage) {
        tit["bf:mainTitle"][0]["@language"] = titleLanguage;
      }
      if (subtitle) {
        tit["bf:subtitle"] = [
          {
            "@value": subtitle,
          },
        ];
        if (titleLanguage) {
          tit["bf:subtitle"][0]["@language"] = titleLanguage;
        }
      }
      h["dce:title"] = [tit];
    }
    if (firstname || lastname || name) {
      let r = {
        "@type": "schema:Person",
      };
      if (firstname) {
        r["schema:givenName"] = [
          {
            "@value": firstname,
          },
        ];
      }
      if (lastname) {
        r["schema:familyName"] = [
          {
            "@value": lastname,
          },
        ];
      }
      if (name) {
        r["schema:name"] = [
          {
            "@value": name,
          },
        ];
      }
      h[role] = [r];
    }
    return h;
  },
  get_json_bf_publication(publisherName, publishingPlace, publishingDate) {
    var h = {
      "@type": "bf:Publication",
    };
    if (publisherName) {
      let pn = {
        "@type": "schema:Organization",
        "schema:name": [
          {
            "@value": publisherName,
          },
        ],
      };
      h["bf:agent"] = [pn];
    }
    if (publishingPlace) {
      let pp = {
        "@type": "schema:Place",
        "skos:prefLabel": [
          {
            "@value": publishingPlace,
          },
        ],
      };
      h["bf:place"] = [pp];
    }
    if (publishingDate) {
      h["bf:date"] = [publishingDate];
    }

    return h;
  },
  get_json_study_plan(name, nameLanguage, notations) {
    var h = {
      "@type": "aaiso:Programme",
    };
    if (name) {
      h["skos:prefLabel"] = [
        {
          "@value": name,
        },
      ];
      if (nameLanguage) {
        h["skos:prefLabel"][0]["@language"] = nameLanguage;
      }
    }
    if (notations) {
      h["skos:notation"] = notations;
    }
    return h;
  },
  get_json_funder(name, nameLanguage, identifiers) {
    var h = {
      "@type": "frapo:FundingAgency",
    };

    if (name) {
      h["skos:prefLabel"] = [
        {
          "@value": name,
        },
      ];
      if (nameLanguage) {
        h["skos:prefLabel"][0]["@language"] = nameLanguage;
      }
    }
    if (identifiers) {
      if (identifiers.length > 0) {
        h["skos:exactMatch"] = identifiers;
      }
    }
    return h;
  },
  validate_object(object) {
    if (!object["@type"]) {
      console.error("JSON-LD validation: missing @type attribute", object);
      return false;
    }
    return true;
  },
  push_object(jsonld, predicate, object) {
    if (this.validate_object(object)) {
      if (!jsonld[predicate]) {
        jsonld[predicate] = [];
      }
      //console.log("in push_object ", jsonld[predicate], "object", object);
      jsonld[predicate].push(object);
    }
  },
  push_literal(jsonld, predicate, value) {
    if (!jsonld[predicate]) {
      jsonld[predicate] = [];
    }
    jsonld[predicate].push(value);
  },
  push_value(jsonld, predicate, valueobject) {
    if (!jsonld[predicate]) {
      jsonld[predicate] = [];
    }
    jsonld[predicate].push(valueobject);
  },
  containerForm2json(formData) {
    var jsonlds = {};
    jsonlds["container"] = {};

    for (var i = 0; i < formData.sections.length; i++) {
      var s = formData.sections[i];
      var jsonldid = "container";

      if (s.type === "accessrights") {
        // handled in PIForm
        continue;
      }
      //dunno why we need this?
      /*if (s.type === "phaidra:Subject") {
       
        jsonldid = "subject-" + i;
        jsonlds[jsonldid] = {
          "@type": "phaidra:Subject"
        };
      }*/

      if (s.type === "member") {
        var n = s.fields[0].nfile;
        // console.log("fileshere", s.fields[0]);
        for (var k = 1; k < n + 1; k++) {
          jsonldid = "member_" + k;

          jsonlds[jsonldid] = {};

          this.fields2jsonFile(jsonlds[jsonldid], k, s);
        }
      } else {
        //console.log("else", s.type);
        jsonldid = "container";
        this.fields2json(jsonlds[jsonldid], s);
      }

      //this.fields2json(jsonlds[jsonldid], s);
    }

    Object.keys(jsonlds).forEach(function(key) {
      if (key.startsWith("subject-")) {
        if (Object.keys(jsonlds[key]).length > 1) {
          if (!jsonlds["container"]["dcterms:subject"]) {
            jsonlds["container"]["dcterms:subject"] = [];
          }
          jsonlds["container"]["dcterms:subject"].push(jsonlds[key]);
        }
        delete jsonlds[key];
      }
    });

    return jsonlds;
  },
  form2json(formData) {
    var i;

    var jsonlds = {};

    for (i = 0; i < formData.sections.length; i++) {
      var s = formData.sections[i];
      var jsonldid;
      if (s.type === "accessrights") {
        // handled in PIForm
        continue;
      }
      if (s.type === "phaidra:Subject") {
        jsonldid = "subject-" + i;
        jsonlds[jsonldid] = {
          "@type": "phaidra:Subject",
        };
        this.fields2json(jsonlds[jsonldid], s);
      } else {
        this.fields2json(jsonlds, s);
      }
    }

    Object.keys(jsonlds).forEach(function(key) {
      if (key.startsWith("subject-")) {
        if (Object.keys(jsonlds[key]).length > 1) {
          if (!jsonlds["dcterms:subject"]) {
            jsonlds["dcterms:subject"] = [];
          }
          jsonlds["dcterms:subject"].push(jsonlds[key]);
        }
        delete jsonlds[key];
      }
    });

    return jsonlds;
  },
  fields2json(jsonld, formData) {
    var myFields = [];
    if (formData.fields) {
      myFields = formData.fields;
      //console.log("fields", myFields);
    } else if (formData.categories) {
      //console.log("categories", formData.categories);
      //we have to loop over categories and then fields
      var cat = formData.categories;
      //console.log("categories", cat);
      for (let j = 0; j < cat.length; j++) {
        //and here we should keep track of which categories there are in?
        myFields = [...myFields, ...cat[j].fields];
      }
    }
    //for (var j = 0; j < myFields.length; j++) {
    //  console.log("fiels", myFields[j].label);
    //}

    for (var j = 0; j < myFields.length; j++) {
      var f = myFields[j];

      //console.log("fields", f.label);
      switch (f.predicate) {
        case "dce:title":
          if (f.title) {
            this.push_object(
              jsonld,
              f.predicate,
              this.get_json_dce_title(f.type, f.title, f.subtitle, f.language)
            );
          }
          break;
        // dates
        case "date":
        case "dcterms:date":
        case "dcterms:created":
        case "dcterms:modified":
        case "dcterms:available":
        case "dcterms:issued":
        case "dcterms:valid":
        case "dcterms:dateAccepted":
        case "dcterms:dateCopyrighted":
        case "dcterms:dateSubmitted":
        case "phaidra:dateAccessioned":
          if (f.value) {
            this.push_literal(jsonld, f.type, f.value);
          }
          break;

        case "bf:note":
          if (f.value) {
            this.push_object(
              jsonld,
              "bf:note",
              this.get_json_abstract(f.value, f.language)
            );
          }

          break;

        case "dcterms:language":
          if (f.value) {
            this.push_literal(jsonld, f.predicate, f.value);
          }
          break;

        case "role":
          if (f.role && (f.firstname || f.lastname || f.email || f.orcid)) {
            this.push_object(
              jsonld,
              f.role,
              this.get_json_role(
                f.type,
                f.firstname,
                f.lastname,
                f.institution,
                f.email,
                f.orcid
              )
            );
          }
          break;

        case "edm:rights":
          //console.log("rights", f.value);
          if (f.value) {
            this.push_literal(jsonld, f.predicate, f.value);
          }
          break;

        case "dce:rights":
        case "dcterms:temporal":
          if (f.value) {
            this.push_value(
              jsonld,
              f.predicate,
              this.get_json_valueobject(f.value, f.language)
            );
          }
          break;

        case "dce:subject":
          if (f.value) {
            this.push_object(
              jsonld,
              f.predicate,
              this.get_json_object(
                [
                  {
                    "@value": f.value,
                    "@language": f.language,
                  },
                ],
                null,
                "skos:Concept"
              )
            );
          }
          break;

        case "dcterms:subject":
          if (f.value) {
            if (f.component === "ext-service") {
              for (var k = 0; k < f.value.length; k++) {
                if (typeof f.value[k] !== "undefined") {
                  if (f.value[k].geneName) {
                    delete f.value[k].geneName;
                  }
                  if (f.value[k].organismName) {
                    delete f.value[k].organismName;
                  }
                  if (f.value[k].proteinRecommendedName) {
                    delete f.value[k].proteinRecommendedName;
                  }
                  if (f.value[k].proteinAlternativeName) {
                    delete f.value[k].proteinAlternativeName;
                  }
                  if (f.value[k].uniprotAccession) {
                    delete f.value[k].uniprotAccession;
                  }
                  if (f.value[k].uniprotName) {
                    delete f.value[k].uniprotName;
                  }
                  if (f.value[k].cellLineName) {
                    delete f.value[k].cellLineName;
                  }
                  if (f.value[k].accessionNumber) {
                    delete f.value[k].accessionNumber;
                  }
                  if (f.value[k].inchiKey) {
                    delete f.value[k].inchiKey;
                  }
                  if (f.value[k].inchi) {
                    delete f.value[k].inchi;
                  }
                  if (f.value[k]["@hierarchicalAncestors"]) {
                    delete f.value[k]["@hierarchicalAncestors"];
                  }

                  f.value[k]["@type"] = f.type;

                  this.push_object(jsonld, f.predicate, f.value[k]);
                }
              }
            } else if (f.component === "selection") {
              for (var m = 0; m < f.value.length; m++) {
                this.push_object(jsonld, f.predicate, f.value[m]);
              }
            } else {
              this.push_object(
                jsonld,
                f.predicate,
                this.get_json_concept([f.value], "", f.type, "", "")
              );
            }
          }

          break;

        case "frapo:isOutputOf":
          /*  if (f.type === "aaiso:Programme") {
            // study plan
            if (f.name || f.notation) {
              this.push_object(
                jsonld,
                f.predicate,
                this.get_json_study_plan(f.name, f.nameLanguage, [f.notation])
              );
            }
          } else {*/
          // project
          if (f.type === "foaf:Project") {
            /*if (f.name || f.identifier || f.description || f.homepage) {
                this.push_object(
                  jsonld,
                  f.predicate,
                  this.get_json_project(
                    f.name,
                    f.nameLanguage,
                    f.description,
                    f.descriptionLanguage,
                    f.identifier ? [f.identifier] : null,
                    f.homepage
                  )
                );
              }*/
            if (f.project) {
              //a bit too much home diy
              if (f.project[0]["skos:prefLabel"][0]["@value"]) {
                //a bit too much home diy
                this.push_object(jsonld, f.predicate, f.project[0]);
              }
            }
            if (f.agency) {
              //a bit too much home diy
              if (f.agency[0]["skos:prefLabel"][0]["@value"]) {
                this.push_object(jsonld, "frapo:hasFundingAgency", f.agency[0]);
              }
            }
          }
          break;
        case "dcterms:type":
          //poor man's method leider for now - make a function
          /*if (f.value) {
            var mycontent = {
              "@type": "skos:Concept",
              "skos:prefLabel": [
                {
                  "@value": "container",
                  "@language": "eng",
                },
              ],
              "skos:exactMatch": [
                "https://pid.phaidra.org/vocabulary/8MY0-BQDQ",
              ],
            };

            this.push_object(jsonld, f.predicate, mycontent);
          }*/
          break;
        case "frapo:hasFundingAgency":
          if (f.name || f.identifier) {
            this.push_object(
              jsonld,
              f.predicate,
              this.get_json_funder(
                f.name,
                f.nameLanguage,
                f.identifier ? [f.identifier] : null
              )
            );
          }
          break;

        default:
          if (f.value) {
            jsonld[f.predicate] = f.value;
          }
          break;
      }
    }

    return jsonld;
  },
  fields2jsonFile(jsonld, i, formData) {
    //console.log("file push", jsonld, i);
    for (var j = 0; j < formData.fields.length; j++) {
      var f = formData.fields[j];

      switch (f.predicate) {
        case "ebucore:filename":
          if (f.fileName[i - 1]) {
            this.push_literal(jsonld, f.predicate, f.fileName[i - 1]);
          }
          if (f.fileDescription[i - 1]) {
            this.push_object(
              jsonld,
              "bf:note",
              this.get_json_general_description(f.fileDescription[i - 1], "eng")
            );
          }
          if (f.fileExtension[i - 1]) {
            this.push_literal(
              jsonld,
              "ebucore:hasMimeType",
              f.fileExtension[i - 1]
            );
          }
          if (f.fileFormat[i - 1]) {
            this.push_literal(jsonld, "dcterms:type", f.fileFormat[i - 1]);
          }

          if (f.fileLicense[i - 1]) {
            this.push_literal(jsonld, "edm:rights", f.fileLicense[i - 1]);
          }
          break;
      }
    }
    return jsonld;
  },
};
