const inputJsonLD = {
  project: {
    "@type": "foaf:Project",
    "skos:prefLabel": [
      {
        "@value": "",
        "@language": "en"
      }
    ],
    "rdfs:comment": [
      {
        "@value": "",
        "@language": "eng"
      }
    ],
    "foaf:homepage": [],
    "skos:notation": []
  },
  agency: {
    "@type": "frapo:FundingAgency",
    "skos:prefLabel": [
      {
        "@value": "",
        "@language": "eng"
      }
    ],
    "skos:exactMatch": []
  },
  institution: {
    "role:aut": [
      {
        "@type": "schema:Organization",
        "schema:name": [
          {
            "@value": ""
          }
        ]
      }
    ]
  },
  language: {
    "@id": "eng",
    "skos:prefLabel": { eng: "English", deu: "Englisch" }
  }
};

export default {
  getFormat: function(prop) {
    return inputJsonLD[prop];
  }
};
