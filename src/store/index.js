import Vue from "vue";
import Vuex from "vuex";
import vocabulary from "./modules/vocabulary";
import metadataInputs from "./modules/metadataInputs";
import initialForm from "./modules/initialForm";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== "production";

export default new Vuex.Store({
  state: {
    settings: {
      instance: {
        api: "",
        solr: "",
        baseurl: "",
        mongo: ""
      },
      global: {
        suggesters: {}
      }
    },
    user: {
      username: "",
      token: "",
      firstname: "",
      lastname: ""
    },
    alerts: {
      alerts: []
    }
  },
  mutations: {
    setAlerts(state, alerts) {
      state.alerts.alerts = alerts;
    },
    clearAlert(state, alert) {
      state.alerts.alerts = state.alerts.alerts.filter(e => e !== alert);
    },
    setUsername(state, username) {
      state.user.username = username;
    },
    clearUser(state) {
      (state.user.username = ""), (state.user.token = "");
    },
    setToken(state, token) {
      state.user.token = token;
    },
    initStore(state) {
      (state.user.username = ""),
        (state.user.token = ""),
        (state.alerts.alerts = []);
    },
    setInstanceApi(state, api) {
      state.settings.instance.api = api;
    },
    setInstanceSolr(state, solr) {
      state.settings.instance.solr = solr;
    },
    setInstanceMongo(state, mongo) {
      state.settings.instance.mongo = mongo;
    },
    setInstancePhaidra(state, baseurl) {
      state.settings.instance.baseurl = baseurl;
    },
    setSuggester(state, data) {
      Vue.set(state.settings.global.suggesters, data.suggester, data.url);
    },
    setLoginData(state, logindata) {
      state.user.firstname = logindata.firstname;
      state.user.lastname = logindata.lastname;
      state.user.email = logindata.email;
    }
  },
  actions: {
    login({ commit, state, rootState }, credentials) {
      return new Promise((resolve, reject) => {
        commit("initStore");

        commit("setUsername", credentials.username);

        fetch(rootState.settings.instance.api + "/signin", {
          method: "GET",
          mode: "cors",
          headers: {
            Authorization:
              "Basic " + btoa(credentials.username + ":" + credentials.password)
          }
        })
          .then(function (response) {
            return response.json();
          })
          .then(function (json) {
            if (json.alerts && json.alerts.length > 0) {
              commit("setAlerts", json.alerts);
            }
            if (json.status === 200) {
              commit("setToken", json["XSRF-TOKEN"]);

              document.cookie = "X-XSRF-TOKEN=" + json["XSRF-TOKEN"];
              // if sign in was successful, get user's data (name, email, etc)
              fetch(
                rootState.settings.instance.api +
                "/directory/user/" +
                state.user.username +
                "/data",
                {
                  method: "GET",
                  mode: "cors",
                  headers: {
                    "X-XSRF-TOKEN": state.user.token
                  }
                }
              )
                .then(function (response) {
                  return response.json();
                })
                .then(function (json) {
                  if (json.alerts && json.alerts.length > 0) {
                    commit("setAlerts", json.alerts);
                  }
                  if (json.status === 200) {
                    commit("setLoginData", {
                      firstname: json.user_data.firstname,
                      lastname: json.user_data.lastname,
                      email: json.user_data.email
                    });
                    resolve();
                  }
                })
                .catch(function (error) {
                  console.log(error);
                  reject();
                });
            }
          })

          .catch(function (error) {
            console.log(error); // eslint-disable-line no-console
            reject();
          });
      });
    },
    logout({ commit, state, rootState }) {
      return new Promise(resolve => {
        fetch(rootState.settings.instance.api + "/signout", {
          method: "GET",
          mode: "cors",
          headers: {
            "X-XSRF-TOKEN": state.token
          }
        })
          .then(function (response) {
            return response.json();
          })
          .then(function (json) {
            commit("initStore");
            if (json.alerts && json.alerts.length > 0) {
              commit("setAlerts", json.alerts);
            }
            resolve();
          })
          .catch(function (error) {
            console.log(error); // eslint-disable-line no-console
            commit("initStore");
            resolve();
          });
      });
    }
  },
  modules: {
    vocabulary,
    metadataInputs,
    initialForm
  },
  strict: debug
});
