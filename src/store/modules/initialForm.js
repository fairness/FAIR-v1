const state = {
  initialForm: [],
};

const mutations = {
  initForm(state, initialForm) {
    state.initialForm = initialForm;
  },

};

export default {
  state,
  mutations
};
