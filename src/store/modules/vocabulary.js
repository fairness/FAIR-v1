import languages from "../../utils/lang";

const state = {
  vocabularies: {
    //pharma bit:
    institution: {
      terms: [
        {
          "@type": "schema:Organization",
          "schema:name": [
            {
              "@value": "University of Vienna"
            }
          ]
        },
        {
          "@type": "schema:Organization",
          "schema:name": [
            {
              "@value": "Technical University of Vienna"
            }
          ]
        },
        {
          "@type": "schema:Organization",
          "schema:name": [
            {
              "@value": "Austrian Institute of Technology"
            }
          ]
        },
        {
          "@type": "schema:Organization",
          "schema:name": [
            {
              "@value": "University of Stockholm"
            }
          ]
        },
        {
          "@type": "schema:Organization",
          "schema:name": [
            {
              "@value": "University of Graz"
            }
          ]
        },
        {
          "@type": "schema:Organization",
          "schema:name": [
            {
              "@value": "Queen Mary, University of London"
            }
          ]
        },
        {
          "@type": "schema:Organization",
          "schema:name": [
            {
              "@value": "Imperial College, London"
            }
          ]
        },
        {
          "@type": "schema:Organization",
          "schema:name": [
            {
              "@value": "Perimeter Institute, Waterloo, Canada"
            }
          ]
        },
        {
          "@type": "schema:Organization",
          "schema:name": [
            {
              "@value": "University of Waterloo, Canada"
            }
          ]
        },
        {
          "@type": "schema:Organization",
          "schema:name": [
            {
              "@value": "Please give me unis you want to see listed"
            }
          ]
        }
      ],
      loaded: true
    },
    agency: {
      terms: [
        {
          "@type": "frapo:FundingAgency",
          "skos:prefLabel": [
            {
              "@value": "Austrian Science Fund (FWF)",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["https://www.grid.ac/institutes/grid.25111.36"]
        },
        {
          "@type": "frapo:FundingAgency",
          "skos:prefLabel": [
            {
              "@value": "European Commission (EU)",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["https://www.grid.ac/institutes/grid.270680.b"]
        }
      ]
    },

    project: {
      terms: [
        {
          "@type": "foaf:Project",
          "skos:prefLabel": [
            {
              "@value": "FAIRness for Life Science Data in Austria",
              "@language": "en"
            }
          ],
          "rdfs:comment": [
            {
              "@value":
                "Management, integration, and reuse of research data are key for innovation and creation of new knowledge. Although we have numerous data sources such as ChEMBL, PubChem, UniProt, and the Protein Data Bank available in the public domain, most of the data created in publicly funded research projects end up in pdf-based supplementary files of publications. At best they are additionally deposited in University repositories such as PHAIDRA, or on the web-site of the principal investigator. Although in principle public, they are quite hidden and not directly accessible for search machines. In order to push the demand for open data, the so called FAIR principles for data (Findable, Accessible, Interoperable, Reuseable) were introduced. These four foundational principles should guide data producers and publishers to ensure transparency, reproducibility, and reusability of data, methods, algorithms, and workflows.",
              "@language": "eng"
            }
          ],
          "foaf:homepage": ["https://pharminfo.univie.ac.at/"],
          "skos:notation": ["ORD 63 Open Research Data"]
        },
        {
          "@type": "foaf:Project",
          "skos:prefLabel": [
            {
              "@value": "SFB35",
              "@language": "en"
            }
          ],
          "rdfs:comment": [
            {
              "@value": "",
              "@language": "eng"
            }
          ],
          "foaf:homepage": [""],
          "skos:notation": [""]
        },
        {
          "@type": "foaf:Project",
          "skos:prefLabel": [
            {
              "@value": "MolTag",
              "@language": "en"
            }
          ],
          "rdfs:comment": [
            {
              "@value": "",
              "@language": "eng"
            }
          ],
          "foaf:homepage": ["https://moltag.univie.ac.at/"],
          "skos:notation": [""]
        },
        {
          "@type": "foaf:Project",
          "skos:prefLabel": [
            {
              "@value": "EU-ToxRisk",
              "@language": "en"
            }
          ],
          "rdfs:comment": [
            {
              "@value": "",
              "@language": "eng"
            }
          ],
          "foaf:homepage": ["https://pharminfo.univie.ac.at/"],
          "skos:notation": [""]
        },
        {
          "@type": "foaf:Project",
          "skos:prefLabel": [
            {
              "@value": "K4DD",
              "@language": "en"
            }
          ],
          "rdfs:comment": [
            {
              "@value": "",
              "@language": "eng"
            }
          ],
          "foaf:homepage": ["https://pharminfo.univie.ac.at/"],
          "skos:notation": [""]
        },
        {
          "@type": "foaf:Project",
          "skos:prefLabel": [
            {
              "@value": "Open PHACTS",
              "@language": "en"
            }
          ],
          "rdfs:comment": [
            {
              "@value": "",
              "@language": "eng"
            }
          ],
          "foaf:homepage": ["https://pharminfo.univie.ac.at/"],
          "skos:notation": [""]
        }
      ]
    },
    bioassay_type: {
      terms: [
        {
          "@type": "pharmaWien:assayType",
          "skos:prefLabel": [
            {
              "@value": "ADMET",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://www.bioassayontology.org/bao#BAO_0000009"]
        },
        {
          "@type": "pharmaWien:assayType",
          "skos:prefLabel": [
            {
              "@value": "Binding type",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://www.bioassayontology.org/bao#BAO_0000041"]
        },
        {
          "@type": "pharmaWien:assayType",
          "skos:prefLabel": [
            {
              "@value": "Functional",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://www.bioassayontology.org/bao#BAO_0000010"]
        },
        {
          "@type": "pharmaWien:assayType",
          "skos:prefLabel": [
            {
              "@value": "Physicochemical",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://www.bioassayontology.org/bao#BAO_0002810"]
        },
        {
          "@type": "pharmaWien:assayType",
          "skos:prefLabel": [
            {
              "@value": "Phenotype characterization",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/ERO_0000923"]
        }
      ],
      loaded: true
    },
    anatomical_category: {
      terms: [
        {
          "@type": "pharmaWien:anatomicalCategory",
          "skos:prefLabel": [
            {
              "@value": "Anatomical entity, cross species",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/UBERON_0001062"]
        },
        {
          "@type": "pharmaWien:anatomicalCategory",
          "skos:prefLabel": [
            { "@value": "Plant anatomical entity", "@language": "eng" }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/PO_0025131"]
        },
        {
          "@type": "pharmaWien:anatomicalCategory",
          "skos:prefLabel": [
            { "@value": "Anatomical entity, fungi", "@language": "eng" }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/FAO_0000001"]
        },
        {
          "@type": "pharmaWien:anatomicalCategory",
          "skos:prefLabel": [
            { "@value": "Anatomical entity, bacteria", "@language": "eng" }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/OMIT_0026581"]
        },
        {
          "@type": "pharmaWien:anatomicalCategory",
          "skos:prefLabel": [
            { "@value": "Viral structures", "@language": "eng" }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/OMIT_0026580"]
        }
      ],
      loaded: true
    },
    life: {
      terms: [
        {
          "@type": "pharmaWien:livingOrg",
          "skos:prefLabel": [
            { "@value": "Living Organisms", "@language": "eng" }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/NCBITaxon_1"]
        },
        {
          "@type": "pharmaWien:livingOrg",
          "skos:prefLabel": [
            { "@value": "Cellosaurus cell-line", "@language": "eng" }
          ],
          "skos:exactMatch": ["https://web.expasy.org/cellosaurus/"]
        }
      ],
      loaded: true
    },
    biological_macromolecule: {
      terms: [
        {
          "@type": "pharmaWien:biologicalMacromolecule",
          "skos:prefLabel": [{ "@value": "Protein", "@language": "eng" }],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/NCIT_C17021"]
        },
        {
          "@type": "pharmaWien:biologicalMacromolecule",
          "skos:prefLabel": [{ "@value": "Gene", "@language": "eng" }],
          "skos:exactMatch": [
            "http://www.bioassayontology.org/bao#BAO_0000582 "
          ]
        },
        {
          "@type": "pharmaWien:biologicalMacromolecule",
          "skos:prefLabel": [
            { "@value": "Polynucleotide", "@language": "eng" }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/CHEBI_15986"]
        },
        {
          "@type": "pharmaWien:biologicalMacromolecule",
          "skos:prefLabel": [
            { "@value": "Protein Family", "@language": "eng" }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/NCIT_C20130"]
        },
        {
          "@type": "pharmaWien:biologicalMacromolecule",
          "skos:prefLabel": [
            { "@value": "Protein Subunit", "@language": "eng" }
          ],
          "skos:exactMatch": ["http://www.bioassayontology.org/bao#"]
        },
        {
          "@type": "pharmaWien:biologicalMacromolecule",
          "skos:prefLabel": [
            { "@value": "Protein Complex", "@language": "eng" }
          ],
          "skos:exactMatch": ["http://www.bioassayontology.org/bao#BAO:0002554"]
        },

        {
          "@type": "pharmaWien:biologicalMacromolecule",
          "skos:prefLabel": [
            { "@value": "Polysaccharide", "@language": "eng" }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/CHEBI:18154"]
        },
        {
          "@type": "pharmaWien:biologicalMacromolecule",
          "skos:prefLabel": [{ "@value": "Lipid", "@language": "eng" }],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/CHEBI:18059"]
        }
      ],

      loaded: true
    },
    assay_target: {
      terms: [
        {
          "@type": "pharmaWien:assayTarget",
          "skos:prefLabel": [
            {
              "@value": "Protein",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/NCIT_C17021"]
        },
        {
          "@type": "pharmaWien:assayTarget",
          "skos:prefLabel": [
            {
              "@value": "Gene",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://www.bioassayontology.org/bao#BAO_0000582"]
        },
        {
          "@type": "pharmaWien:assayTarget",
          "skos:prefLabel": [
            {
              "@value": "Nucleic Acid",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/CHEBI_33696"]
        },

        {
          "@type": "pharmaWien:assayTarget",
          "skos:prefLabel": [
            {
              "@value": "Protein Family",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/NCIT_C20130"]
        },
        {
          "@type": "pharmaWien:assayTarget",
          "skos:prefLabel": [
            {
              "@value": "Protein Subunit",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://www.bioassayontology.org/bao#BAO_0002555"]
        },
        {
          "@type": "pharmaWien:assayTarget",
          "skos:prefLabel": [
            {
              "@value": "Protein Complex",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://www.bioassayontology.org/bao#BAO_0002554"]
        },

        {
          "@type": "pharmaWien:assayTarget",
          "skos:prefLabel": [
            {
              "@value": "Carbohydrate",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/CHEBI_16646"]
        },

        {
          "@type": "pharmaWien:assayTarget",
          "skos:prefLabel": [
            {
              "@value": "Lipid",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/CHEBI_18059"]
        },

        {
          "@type": "pharmaWien:assayTarget",
          "skos:prefLabel": [
            {
              "@value": "Small Molecule",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/NCIT_C48809"]
        },
        {
          "@type": "pharmaWien:assayTarget",
          "skos:prefLabel": [
            {
              "@value": "Organism",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/NCIT_C14250"]
        },
        {
          "@type": "pharmaWien:assayTarget",
          "skos:prefLabel": [
            {
              "@value": "Cultured Cell (cellosaurus)",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/CL_0000010"]
        },
        {
          "@type": "pharmaWien:assayTarget",
          "skos:prefLabel": [
            {
              "@value": "Cellular anatomical entity",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/GO_0110165"]
        },

        {
          "@type": "pharmaWien:assayTarget",
          "skos:prefLabel": [
            {
              "@value": "Tissue",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/NCIT:C12801"]
        }
      ],

      loaded: true
    },
    assay_organism: {
      terms: [
        {
          "@type": "pharmaWien:assayOrg",
          "skos:prefLabel": [
            {
              "@value": "Cellular organism",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/NCBITaxon_131567"]
        },
        {
          "@type": "pharmaWien:assayOrg",
          "skos:prefLabel": [
            {
              "@value": "Viruses",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/NCBITaxon_10239"]
        },
        {
          "@type": "pharmaWien:assayOrg",
          "skos:prefLabel": [
            {
              "@value": "Viroids",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/NCBITaxon_12884"]
        }
      ],
      loaded: true
    },
    assayTarget_organism: {
      terms: [
        {
          "@type": "pharmaWien:assayOrg_assayTarget",
          "skos:prefLabel": [
            {
              "@value": "Cellular organism",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/NCBITaxon_131567"]
        },
        {
          "@type": "pharmaWien:assayOrg_assayTarget",
          "skos:prefLabel": [
            {
              "@value": "Viruses",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/NCBITaxon_10239"]
        },
        {
          "@type": "pharmaWien:assayOrg_assayTarget",
          "skos:prefLabel": [
            {
              "@value": "Viroids",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://purl.obolibrary.org/obo/NCBITaxon_12884"]
        }
      ],
      loaded: true
    },
    assay_detection: {
      terms: [
        {
          "@type": "pharmaWien:assayDetection",
          "skos:prefLabel": [
            {
              "@value": "Physical detection method",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://www.bioassayontology.org/bao#BAO_0000035"]
        },
        {
          "@type": "pharmaWien:assayDetection",
          "skos:prefLabel": [
            {
              "@value": "Detection instrument",
              "@language": "eng"
            }
          ],
          "skos:exactMatch": ["http://www.bioassayontology.org/bao#BAO_0000697"]
        }
      ],

      loaded: true
    },
    "mime-types": {
      terms: [
        { "@id": "image/jpeg", "skos:prefLabel": { eng: "JPG/JPEG" } },
        { "@id": "image/tiff", "skos:prefLabel": { eng: "TIFF" } },
        { "@id": "image/gif", "skos:prefLabel": { eng: "GIF" } },
        { "@id": "image/png", "skos:prefLabel": { eng: "PNG" } },
        { "@id": "image/x-ms-bmp", "skos:prefLabel": { eng: "BMP" } },
        { "@id": "audio/wav", "skos:prefLabel": { eng: "WAVE" } },
        { "@id": "audio/mpeg", "skos:prefLabel": { eng: "MP3" } },
        { "@id": "audio/flac", "skos:prefLabel": { eng: "FLAC" } },
        { "@id": "audio/ogg", "skos:prefLabel": { eng: "Ogg" } },
        { "@id": "application/pdf", "skos:prefLabel": { eng: "PDF" } },
        { "@id": "video/mpeg", "skos:prefLabel": { eng: "MPEG" } },
        { "@id": "video/avi", "skos:prefLabel": { eng: "AVI" } },
        { "@id": "video/mp4", "skos:prefLabel": { eng: "MPEG-4" } },
        { "@id": "video/quicktime", "skos:prefLabel": { eng: "Quicktime" } },
        { "@id": "video/x-matroska", "skos:prefLabel": { eng: "MKV" } },
        { "@id": "text/plain", "skos:prefLabel": { eng: "TXT" } },
        { "@id": "text/csv", "skos:prefLabel": { eng: "CSV" } },
        {
          "@id":
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          "skos:prefLabel": { eng: "XLSX" }
        },
        { "@id": "", "skos:prefLabel": { eng: "MPEG-4" } },
        { "@id": "", "skos:prefLabel": { eng: "Quicktime" } },
        { "@id": "", "skos:prefLabel": { eng: "MKV" } },
        {
          "@id": "chemical / x - mdl - sdfile",
          "skos:prefLabel": { eng: "X/MDL/SDFILE" }
        }
      ],
      loaded: true
    },

    "file-types": {
      terms: [
        { "@id": "Image", "skos:prefLabel": { eng: "image" } },
        { "@id": "Document", "skos:prefLabel": { eng: "document" } },
        { "@id": "Table", "skos:prefLabel": { eng: "table" } },
        { "@id": "Script", "skos:prefLabel": { eng: "script" } },
        {
          "@id": "Chemical file format",
          "skos:prefLabel": { eng: "chemical file format" }
        },
        { "@id": "Folder", "skos:prefLabel": { eng: "folder" } }
      ],
      loaded: true
    },

    pool: {
      terms: [
        { "@id": "Bibliothek", "skos:prefLabel": { eng: "Bibliothek" } },
        {
          "@id": "Institut für Romanistik",
          "skos:prefLabel": { eng: "Institut für Romanistik" }
        },
        { "@id": "privat", "skos:prefLabel": { eng: "privat" } }
      ],
      loaded: true
    },
    datepredicate: {
      terms: [
        {
          "@id": "dcterms:date",
          "skos:prefLabel": { eng: "Date", deu: "Datum" }
        },
        {
          "@id": "dcterms:created",
          "skos:prefLabel": { eng: "Date created", deu: "Date created" }
        },
        {
          "@id": "dcterms:modified",
          "skos:prefLabel": { eng: "Date modified", deu: "Date modified" }
        },
        {
          "@id": "dcterms:available",
          "skos:prefLabel": { eng: "Date available", deu: "Date available" }
        },
        {
          "@id": "dcterms:issued",
          "skos:prefLabel": { eng: "Date issued", deu: "Date issued" }
        },
        {
          "@id": "dcterms:valid",
          "skos:prefLabel": { eng: "Date valid", deu: "Date valid" }
        },
        {
          "@id": "dcterms:dateAccepted",
          "skos:prefLabel": { eng: "Date accepted", deu: "Date accepted" }
        },
        {
          "@id": "dcterms:dateCopyrighted",
          "skos:prefLabel": { eng: "Date copyrighted", deu: "Date copyrighted" }
        },
        {
          "@id": "dcterms:dateSubmitted",
          "skos:prefLabel": { eng: "Date submitted", deu: "Date submitted" }
        },
        {
          "@id": "rdau:P60071",
          "skos:prefLabel": {
            eng: "Date of production",
            deu: "Produktionsdatum"
          }
        },
        {
          "@id": "phaidra:dateAccessioned",
          "skos:prefLabel": { eng: "Date accessioned", deu: "Eingangsdatum" }
        }
      ],
      loaded: true
    },
    placepredicate: {
      terms: [
        {
          "@id": "dcterms:spatial",
          "skos:prefLabel": { eng: "Depicted/Represented place" }
        },
        {
          "@id": "vra:placeOfCreation",
          "skos:prefLabel": { eng: "Place of creation" }
        },
        { "@id": "vra:placeOfSite", "skos:prefLabel": { eng: "Place of site" } }
      ],
      loaded: true
    },
    citationpredicate: {
      terms: [
        { "@id": "cito:cites", "skos:prefLabel": { eng: "Cites" } },
        { "@id": "cito:isCitedBy", "skos:prefLabel": { eng: "Is cited by" } }
      ],
      loaded: true
    },
    rolepredicate: {
      terms: [
        {
          "@id": "role:asn",
          "skos:prefLabel": {
            eng: "Associated name",
            deu: "Assoziierter Name",
            ita: "Associated name"
          }
        },
        {
          "@id": "role:att",
          "skos:prefLabel": {
            eng: "Attributed name",
            deu: "zugeschriebene/r AutorIn",
            ita: "Nome attribuito"
          }
        },
        {
          "@id": "role:aut",
          "skos:prefLabel": { eng: "Author", deu: "Autor*in", ita: "Author" }
        },

        {
          "@id": "role:com",
          "skos:prefLabel": {
            eng: "Compiler",
            deu: "HerausgeberIn einer Sammlung",
            ita: "Compilatore"
          }
        },

        {
          "@id": "role:csl",
          "skos:prefLabel": {
            eng: "Consultant",
            deu: "Fachliche Beratung / FachberaterIn",
            ita: "Consultant"
          }
        },
        {
          "@id": "role:csp",
          "skos:prefLabel": {
            eng: "Consultant to a project",
            deu: "ProjektberaterIn",
            ita: "Consultant to a project"
          }
        },

        {
          "@id": "role:ctb",
          "skos:prefLabel": {
            eng: "Contributor",
            deu: "Beitragende/r",
            ita: "Contributor"
          }
        },

        {
          "@id": "role:cph",
          "skos:prefLabel": {
            eng: "Copyright holder",
            deu: "InhaberIn des Copyright",
            ita: "Copyright holder"
          }
        },

        {
          "@id": "role:dtc",
          "skos:prefLabel": {
            eng: "Data contributor",
            deu: "DatenlieferantIn",
            ita: "Data contributor"
          }
        },
        {
          "@id": "role:dtm",
          "skos:prefLabel": {
            eng: "Data manager",
            deu: "DatenmanagerIn",
            ita: "Gestore di dati"
          }
        },
        {
          "@id": "role:datasupplier",
          "skos:prefLabel": {
            eng: "Data Supplier",
            deu: "DatenlieferantIn",
            ita: "Fornitore dei dati"
          }
        },

        {
          "@id": "role:dgs",
          "skos:prefLabel": {
            eng: "Degree supervisor",
            deu: "Doktorvater / BetreuerIn einer Akademischen Abschlußarbeit",
            ita: "Degree supervisor"
          }
        },

        {
          "@id": "role:dpt",
          "skos:prefLabel": {
            eng: "Depositor",
            deu: "Depositor",
            ita: "Depositor"
          }
        },

        {
          "@id": "role:dis",
          "skos:prefLabel": {
            eng: "Dissertant",
            deu: "DissertantIn",
            ita: "Tesista"
          }
        },

        {
          "@id": "role:edt",
          "skos:prefLabel": { eng: "Editor", deu: "EditorIn", ita: "Curatore" }
        },

        {
          "@id": "role:fld",
          "skos:prefLabel": {
            eng: "Field director",
            deu: "LeiterIn von Feldforschung",
            ita: "Field director"
          }
        },

        {
          "@id": "role:ldr",
          "skos:prefLabel": {
            eng: "Laboratory director",
            deu: "LaborleiterIn",
            ita: "Laboratory director"
          }
        },

        {
          "@id": "role:mdc",
          "skos:prefLabel": {
            eng: "Metadata contact",
            deu: "Metadata contact",
            ita: "Metadata contact"
          }
        },

        {
          "@id": "role:pta",
          "skos:prefLabel": {
            eng: "Patent applicant",
            deu: "PatentanmelderIn",
            ita: "Patent applicant"
          }
        },
        {
          "@id": "role:pth",
          "skos:prefLabel": {
            eng: "Patent holder",
            deu: "PatentinhaberIn",
            ita: "Patent holder"
          }
        },

        {
          "@id": "role:prg",
          "skos:prefLabel": {
            eng: "Programmer",
            deu: "ProgrammiererIn",
            ita: "Programmer"
          }
        },
        {
          "@id": "role:pdr",
          "skos:prefLabel": {
            eng: "Project director",
            deu: "PrjektleiterIn",
            ita: "Project director"
          }
        },

        {
          "@id": "role:rth",
          "skos:prefLabel": {
            eng: "Research team head",
            deu: "Projektverantwortliche/r",
            ita: "Direttore della ricerca"
          }
        },
        {
          "@id": "role:rtm",
          "skos:prefLabel": {
            eng: "Research team member",
            deu: "ProjektmitarbeiterIn",
            ita: "Membro di un gruppo di ricerca"
          }
        },
        {
          "@id": "role:res",
          "skos:prefLabel": {
            eng: "Researcher",
            deu: "ForscherIn",
            ita: "Ricercatore"
          }
        },

        {
          "@id": "role:sad",
          "skos:prefLabel": {
            eng: "Scientific advisor",
            deu: "wissenschaftliche BeraterIn",
            ita: "Consulente scientifico"
          }
        },

        {
          "@id": "role:ths",
          "skos:prefLabel": {
            eng: "Thesis advisor",
            deu: "DissertationsbetreuerIn",
            ita: "Relatore"
          }
        },

        {
          "@id": "role:uploader",
          "skos:prefLabel": {
            eng: "Uploader",
            deu: "Uploader",
            ita: "Uploader"
          }
        }
      ],
      loaded: true
    },
    lang: {
      terms: [],
      loaded: false
    },
    mimetypes: {
      terms: [
        { "@id": "image/jpeg", "skos:prefLabel": { eng: "JPG/JPEG" } },
        { "@id": "image/tiff", "skos:prefLabel": { eng: "TIFF" } },
        { "@id": "image/gif", "skos:prefLabel": { eng: "GIF" } },
        { "@id": "image/png", "skos:prefLabel": { eng: "PNG" } },
        { "@id": "image/x-ms-bmp", "skos:prefLabel": { eng: "BMP" } },
        { "@id": "audio/wav", "skos:prefLabel": { eng: "WAVE" } },
        { "@id": "audio/mpeg", "skos:prefLabel": { eng: "MP3" } },
        { "@id": "audio/flac", "skos:prefLabel": { eng: "FLAC" } },
        { "@id": "audio/ogg", "skos:prefLabel": { eng: "Ogg" } },
        { "@id": "application/pdf", "skos:prefLabel": { eng: "PDF" } },
        { "@id": "video/mpeg", "skos:prefLabel": { eng: "MPEG" } },
        { "@id": "video/avi", "skos:prefLabel": { eng: "AVI" } },
        { "@id": "video/mp4", "skos:prefLabel": { eng: "MPEG-4" } },
        { "@id": "video/quicktime", "skos:prefLabel": { eng: "Quicktime" } },
        { "@id": "video/x-matroska", "skos:prefLabel": { eng: "MKV" } },
        {
          "@id": "application/x-iso9660-image",
          "skos:prefLabel": { eng: "ISO" }
        }
      ],
      loaded: true
    },
    licenses: {
      terms: [
        {
          "@id": "http://rightsstatements.org/vocab/InC/1.0/",
          "skos:prefLabel": { eng: "© All rights reserved" },
          img: "cc-by.png"
        },
        {
          "@id": "http://creativecommons.org/licenses/by/4.0/",
          "skos:prefLabel": { eng: "CC BY 4.0 International (Attribution)" },
          img: "cc-by.png"
        },
        {
          "@id": "http://creativecommons.org/licenses/by-nc/4.0/",
          "skos:prefLabel": {
            eng: "CC BY-NC 4.0 International (Attribution-NonCommercial)"
          },
          img: "cc-by-nc.png"
        },
        {
          "@id": "http://creativecommons.org/licenses/by-nc-nd/4.0/",
          "skos:prefLabel": {
            eng:
              "CC BY-NC-ND 4.0 International (Attribution-NonCommercial-NoDerivatives)"
          },
          img: "cc-by-nc-nd.png"
        },
        {
          "@id": "http://creativecommons.org/licenses/by-nc-sa/4.0/",
          "skos:prefLabel": {
            eng:
              "CC BY-NC-SA 4.0 International (Attribution-NonCommercial-ShareAlike)"
          },
          img: "cc-by-nc-sa.png"
        },
        {
          "@id": "http://creativecommons.org/licenses/by-nd/4.0/",
          "skos:prefLabel": {
            eng: "CC BY-ND 4.0 International (Attribution-NoDerivatives)"
          },
          img: "cc-by-nd.png"
        },
        {
          "@id": "http://creativecommons.org/licenses/by-sa/4.0/",
          "skos:prefLabel": {
            eng: "CC BY-SA 4.0 International (Attribution-ShareAlike)"
          },
          img: "cc-by-sa.png"
        }
      ],
      loaded: true
    },
    uncefact: {
      terms: [
        { "@id": "MTR", "skos:prefLabel": { eng: "m" } },
        { "@id": "CMT", "skos:prefLabel": { eng: "cm" } },
        { "@id": "MMT", "skos:prefLabel": { eng: "mm" } }
      ],
      loaded: true
    },
    carriertype: {
      terms: [
        {
          "@id": "https://pid.phaidra.org/vocabulary/2FTX-ZPZV",
          "skos:prefLabel": { eng: "ADAT" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/A3BG-65F5",
          "skos:prefLabel": { eng: "CD" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/4CQF-7HHF",
          "skos:prefLabel": { eng: "DAT" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/HXSS-NBZ4",
          "skos:prefLabel": { eng: "audiocassette" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/X627-FCV9",
          "skos:prefLabel": { eng: "tape" }
        }
      ],
      loaded: true
    },
    resourcetype: {
      terms: [
        {
          "@id": "https://pid.phaidra.org/vocabulary/44TN-P1S0",
          "skos:prefLabel": { eng: "image" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/69ZZ-2KGX",
          "skos:prefLabel": { eng: "text" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/GXS7-ENXJ",
          "skos:prefLabel": { eng: "collection" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/B0Y6-GYT8",
          "skos:prefLabel": { eng: "video" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/7AVS-Y482",
          "skos:prefLabel": { eng: "data" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/8YB5-1M0J",
          "skos:prefLabel": { eng: "sound" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/8MY0-BQDQ",
          "skos:prefLabel": { eng: "container" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/T8GH-F4V8",
          "skos:prefLabel": { eng: "resource" }
        }
      ],
      loaded: true
    },
    genre: {
      terms: [
        {
          "@id": "https://pid.phaidra.org/vocabulary/9E94-E3F8",
          "skos:prefLabel": { eng: "Diplomarbeit" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/P2YP-BMND",
          "skos:prefLabel": { eng: "Masterarbeit" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/1PHE-7VMS",
          "skos:prefLabel": { eng: "Dissertation" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/ST05-F6SP",
          "skos:prefLabel": { eng: "Magisterarbeit" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/9ZSV-CVJH",
          "skos:prefLabel": { eng: "Habilitation" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/H1TF-SDX1",
          "skos:prefLabel": { eng: "Master-Thesis (ULG)" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/QNV1-N1EC",
          "skos:prefLabel": { eng: "action" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/31DA-295K",
          "skos:prefLabel": { eng: "anime" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/DB5C-1Y4H",
          "skos:prefLabel": { eng: "biopic" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/MKKZ-BH2Q",
          "skos:prefLabel": { eng: "discussion" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/WVGH-KT47",
          "skos:prefLabel": { eng: "documentary film" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/XFDY-E13E",
          "skos:prefLabel": { eng: "drama" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/GZQE-YK3K",
          "skos:prefLabel": { eng: "fantasy" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/KM7A-FYPP",
          "skos:prefLabel": { eng: "television film" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/MN1Y-YFCF",
          "skos:prefLabel": { eng: "historical film" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/G2VQ-GEEK",
          "skos:prefLabel": { eng: "horror" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/GFM4-2J48",
          "skos:prefLabel": { eng: "comedy" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/NQVM-6B2Y",
          "skos:prefLabel": { eng: "crime" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/BPAJ-NQ8N",
          "skos:prefLabel": { eng: "short film" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/AHWA-YKFH",
          "skos:prefLabel": { eng: "romance film" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/7RZF-5216",
          "skos:prefLabel": { eng: "musical" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/A1B4-K5MK",
          "skos:prefLabel": { eng: "newscast" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/B4R8-Z419",
          "skos:prefLabel": { eng: "romance" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/XZ5S-JEJ5",
          "skos:prefLabel": { eng: "satire" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/YV6T-SWAF",
          "skos:prefLabel": { eng: "science fiction" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/A8HT-N1QB",
          "skos:prefLabel": { eng: "series" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/1VZT-KE1S",
          "skos:prefLabel": { eng: "thriller" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/PCK6-NYPG",
          "skos:prefLabel": { eng: "tragicomedy" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/2PV5-5V2H",
          "skos:prefLabel": { eng: "entertainment" }
        }
      ],
      loaded: true
    },
    thesisgenre: {
      terms: [
        {
          "@id": "https://pid.phaidra.org/vocabulary/9E94-E3F8",
          "skos:prefLabel": { eng: "Diplomarbeit" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/P2YP-BMND",
          "skos:prefLabel": { eng: "Masterarbeit" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/1PHE-7VMS",
          "skos:prefLabel": { eng: "Dissertation" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/ST05-F6SP",
          "skos:prefLabel": { eng: "Magisterarbeit" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/9ZSV-CVJH",
          "skos:prefLabel": { eng: "Habilitation" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/H1TF-SDX1",
          "skos:prefLabel": { eng: "Master-Thesis (ULG)" }
        }
      ],
      loaded: true
    },
    moviegenre: {
      terms: [
        {
          "@id": "https://pid.phaidra.org/vocabulary/QNV1-N1EC",
          "skos:prefLabel": { eng: "action" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/31DA-295K",
          "skos:prefLabel": { eng: "anime" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/DB5C-1Y4H",
          "skos:prefLabel": { eng: "biopic" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/MKKZ-BH2Q",
          "skos:prefLabel": { eng: "discussion" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/WVGH-KT47",
          "skos:prefLabel": { eng: "documentary film" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/XFDY-E13E",
          "skos:prefLabel": { eng: "drama" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/GZQE-YK3K",
          "skos:prefLabel": { eng: "fantasy" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/KM7A-FYPP",
          "skos:prefLabel": { eng: "television film" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/MN1Y-YFCF",
          "skos:prefLabel": { eng: "historical film" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/G2VQ-GEEK",
          "skos:prefLabel": { eng: "horror" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/GFM4-2J48",
          "skos:prefLabel": { eng: "comedy" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/NQVM-6B2Y",
          "skos:prefLabel": { eng: "crime" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/BPAJ-NQ8N",
          "skos:prefLabel": { eng: "short film" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/AHWA-YKFH",
          "skos:prefLabel": { eng: "romance film" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/7RZF-5216",
          "skos:prefLabel": { eng: "musical" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/A1B4-K5MK",
          "skos:prefLabel": { eng: "newscast" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/B4R8-Z419",
          "skos:prefLabel": { eng: "romance" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/XZ5S-JEJ5",
          "skos:prefLabel": { eng: "satire" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/YV6T-SWAF",
          "skos:prefLabel": { eng: "science fiction" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/A8HT-N1QB",
          "skos:prefLabel": { eng: "series" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/1VZT-KE1S",
          "skos:prefLabel": { eng: "thriller" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/PCK6-NYPG",
          "skos:prefLabel": { eng: "tragicomedy" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/2PV5-5V2H",
          "skos:prefLabel": { eng: "entertainment" }
        }
      ],
      loaded: true
    },
    objecttype: {
      terms: [
        {
          "@id": "https://pid.phaidra.org/vocabulary/VKA6-9XTY",
          "skos:prefLabel": { eng: "journal article" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/A52A-CWMM",
          "skos:prefLabel": { eng: "map" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/47QB-8QF1",
          "skos:prefLabel": { eng: "book" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/GBWA-JJP8",
          "skos:prefLabel": { eng: "letter" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/EWZ9-3MPH",
          "skos:prefLabel": { eng: "musical composition" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/8A6X-FKB1",
          "skos:prefLabel": { eng: "musical notation" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/7CAB-P987",
          "skos:prefLabel": { eng: "photograph" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/YBTD-Q94N",
          "skos:prefLabel": { eng: "railroad bridge" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/QT7P-HNZB",
          "skos:prefLabel": { eng: "tribe (kinship group)" }
        }
      ],
      loaded: true
    },
    technique: {
      terms: [
        {
          "@id": "https://pid.phaidra.org/vocabulary/NZ42-TTZT",
          "skos:prefLabel": { eng: "black-and-white photography" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/DC1W-JWNP",
          "skos:prefLabel": { eng: "color photography" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/AH0S-F3BV",
          "skos:prefLabel": { eng: "black-and-white film" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/K818-FSM5",
          "skos:prefLabel": { eng: "color film" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/748F-SQW9",
          "skos:prefLabel": { eng: "silent film" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/1K09-VXQ4",
          "skos:prefLabel": { eng: "sound film" }
        }
      ],
      loaded: true
    },
    material: {
      terms: [
        {
          "@id": "https://pid.phaidra.org/vocabulary/CRGV-097N",
          "skos:prefLabel": { eng: "black marble" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/EXRJ-GCYG",
          "skos:prefLabel": { eng: "shampoo" }
        }
      ],
      loaded: true
    },
    reproduction: {
      terms: [
        {
          "@id": "https://pid.phaidra.org/vocabulary/AYRE-RQAS",
          "skos:prefLabel": { eng: "original" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/BD33-7WA2",
          "skos:prefLabel": { eng: "copy" }
        }
      ],
      loaded: true
    },
    audience: {
      terms: [
        {
          "@id": "https://pid.phaidra.org/vocabulary/TEPR-J4EZ",
          "skos:prefLabel": { eng: "FSK ab 0 freigegeben" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/7ANY-9744",
          "skos:prefLabel": { eng: "FSK ab 6 freigegeben" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/4DQY-TNPT",
          "skos:prefLabel": { eng: "FSK ab 12 freigegeben" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/HSDH-MD0J",
          "skos:prefLabel": { eng: "FSK ab 16 freigegeben" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/F2VP-9Z07",
          "skos:prefLabel": { eng: "FSK ab 18 freigegeben" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/C2TK-3DTQ",
          "skos:prefLabel": { eng: "Freigegeben gemäß §14 JuSchG FSK" }
        }
      ],
      loaded: true
    },
    regionalencoding: {
      terms: [
        {
          "@id": "https://pid.phaidra.org/vocabulary/AR9M-B9J4",
          "skos:prefLabel": { eng: "1" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/6Z5R-XEG2",
          "skos:prefLabel": { eng: "2" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/2YZZ-TX6M",
          "skos:prefLabel": { eng: "3" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/36BC-K989",
          "skos:prefLabel": { eng: "4" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/ADS3-D2RC",
          "skos:prefLabel": { eng: "5" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/9NQT-YAJ4",
          "skos:prefLabel": { eng: "6" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/QN10-XAKZ",
          "skos:prefLabel": { eng: "7" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/KE1K-8NT7",
          "skos:prefLabel": { eng: "8" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/GSVQ-6H7P",
          "skos:prefLabel": { eng: "A/1" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/149W-4F0N",
          "skos:prefLabel": { eng: "B/2" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/VHCV-2WY3",
          "skos:prefLabel": { eng: "C/3" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/3MQF-RDQQ",
          "skos:prefLabel": { eng: "region free" }
        }
      ],
      loaded: true
    },
    dceformat: {
      terms: [
        {
          "@id": "https://pid.phaidra.org/vocabulary/J6JG-69V6",
          "skos:prefLabel": { eng: "3gp" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/3F67-KMTM",
          "skos:prefLabel": { eng: "AAC+" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/7A81-FXCX",
          "skos:prefLabel": { eng: "Barco Auro" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/FRJJ-4376",
          "skos:prefLabel": { eng: "DTS" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/EHF7-FEAP",
          "skos:prefLabel": { eng: "DTS 96/24" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/T5SX-Z04Y",
          "skos:prefLabel": { eng: "DTS Discrete 5.1" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/9FGJ-Z8DH",
          "skos:prefLabel": { eng: "DTS ES Discrete 6.1" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/ESQT-3YY5",
          "skos:prefLabel": { eng: "DTS ES Matrix 6.1" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/348K-MZZ6",
          "skos:prefLabel": { eng: "DTS HD" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/EN75-Q4HC",
          "skos:prefLabel": { eng: "DTS NEO:6" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/T7Q0-M2FS",
          "skos:prefLabel": { eng: "DTS++" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/28X4-0935",
          "skos:prefLabel": { eng: "DTS:X" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/3W6M-5MP3",
          "skos:prefLabel": { eng: "Datasat" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/MCQ6-HPAH",
          "skos:prefLabel": { eng: "Digital Surround 7.1" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/8T8J-936P",
          "skos:prefLabel": { eng: "Dolby Atmos" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/9SHY-VVN6",
          "skos:prefLabel": { eng: "Dolby Digital" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/61PF-1NEJ",
          "skos:prefLabel": { eng: "Dolby Digital 5.1" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/GHEV-3W1J",
          "skos:prefLabel": { eng: "Dolby Digital EX" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/VN51-WRAF",
          "skos:prefLabel": { eng: "Dolby Digital Plus" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/8RFW-88Q3",
          "skos:prefLabel": { eng: "Dolby Pro Logic II" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/55WB-XQ4P",
          "skos:prefLabel": { eng: "Dolby SR-D" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/2JPY-C523",
          "skos:prefLabel": { eng: "Dolby SR-D-EX" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/XZNA-QKBP",
          "skos:prefLabel": { eng: "Dolby Stereo" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/W56N-VX1X",
          "skos:prefLabel": { eng: "Dolby Surround" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/DCZS-VZ7X",
          "skos:prefLabel": { eng: "Dolby TrueHD" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/SDE9-JMJJ",
          "skos:prefLabel": { eng: "Dolby-SR" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/1DE8-XDG2",
          "skos:prefLabel": { eng: "MPEG-4 ALS" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/K9BD-K8GP",
          "skos:prefLabel": { eng: "Mono" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/2BCB-S1B5",
          "skos:prefLabel": { eng: "SDDS" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/7FMD-95WA",
          "skos:prefLabel": { eng: "Stereo" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/KCDR-Q08F",
          "skos:prefLabel": { eng: "Stereo 2.0" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/DBKT-3BQ3",
          "skos:prefLabel": { eng: "Surround 5.1" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/G1KV-MAFP",
          "skos:prefLabel": { eng: "VCD" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/RQK2-8156",
          "skos:prefLabel": { eng: "aif" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/52G8-44YX",
          "skos:prefLabel": { eng: "ape" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/C5G1-JQQZ",
          "skos:prefLabel": { eng: "asf" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/AFZ7-4AY9",
          "skos:prefLabel": { eng: "au" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/KF60-K40F",
          "skos:prefLabel": { eng: "avi" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/44YA-6HGK",
          "skos:prefLabel": { eng: "divx" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/GDTY-V9H4",
          "skos:prefLabel": { eng: "dv" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/GW3R-K19G",
          "skos:prefLabel": { eng: "evo" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/D3GB-MPSY",
          "skos:prefLabel": { eng: "fla" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/E2EJ-277K",
          "skos:prefLabel": { eng: "flac" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/29PG-5AKN",
          "skos:prefLabel": { eng: "flv" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/2CRY-PD3C",
          "skos:prefLabel": { eng: "ggf" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/KPT0-47XA",
          "skos:prefLabel": { eng: "m2ts" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/YK2W-SAFC",
          "skos:prefLabel": { eng: "m4a" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/S9PM-AGHF",
          "skos:prefLabel": { eng: "mac" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/9594-QH49",
          "skos:prefLabel": { eng: "mka" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/RRWW-7W5N",
          "skos:prefLabel": { eng: "mkv" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/15DS-TAGX",
          "skos:prefLabel": { eng: "mp3" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/F487-H20C",
          "skos:prefLabel": { eng: "mp3HD" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/AFA0-APVK",
          "skos:prefLabel": { eng: "mpeg" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/DT3X-PH6D",
          "skos:prefLabel": { eng: "mpeg-1" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/B008-MRZD",
          "skos:prefLabel": { eng: "mpeg-2" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/PQ4H-JT2Y",
          "skos:prefLabel": { eng: "mpeg-4" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/26M9-M5MR",
          "skos:prefLabel": { eng: "mpg" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/NZKM-SH76",
          "skos:prefLabel": { eng: "mts" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/T59Z-PCFE",
          "skos:prefLabel": { eng: "mxf" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/TR4G-6P0B",
          "skos:prefLabel": { eng: "ogg" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/7ZVE-WQQ2",
          "skos:prefLabel": { eng: "ra" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/J5QX-S86N",
          "skos:prefLabel": { eng: "ram" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/P66F-9ERB",
          "skos:prefLabel": { eng: "rm" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/BJDK-FC30",
          "skos:prefLabel": { eng: "snd" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/N0QE-B24V",
          "skos:prefLabel": { eng: "vob" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/X2ZR-7C1F",
          "skos:prefLabel": { eng: "voc" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/QYGZ-K8W4",
          "skos:prefLabel": { eng: "wav" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/6S1H-S5GF",
          "skos:prefLabel": { eng: "webm" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/783J-J5PD",
          "skos:prefLabel": { eng: "wma" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/7B2N-07A7",
          "skos:prefLabel": { eng: "wmv" }
        },
        {
          "@id": "https://pid.phaidra.org/vocabulary/VTY6-58WQ",
          "skos:prefLabel": { eng: "xvid" }
        }
      ],
      loaded: true
    }
  }
};

const mutations = {
  setLangTerms(state, data) {
    if (state.vocabularies["lang"]["loaded"] === false) {
      state.vocabularies["lang"]["terms"] = data;
      state.vocabularies["lang"]["loaded"] = true;
    }
  }
};

const actions = {
  loadLanguages({ commit }) {
    commit("setLangTerms", languages.get_lang());
  }
};

const getters = {
  getLocalizedTermLabel: state => (voc, id, lang) => {
    var terms = state.vocabularies[voc].terms;
    for (var i = 0; i < terms.length; i++) {
      if (terms[i]["@id"] === id) {
        return terms[i]["skos:prefLabel"][lang]
          ? terms[i]["skos:prefLabel"][lang]
          : terms[i]["skos:prefLabel"]["eng"];
      }
    }
  },
  getTerm: state => (voc, id) => {
    var terms = state.vocabularies[voc].terms;
    for (var i = 0; i < terms.length; i++) {
      if (terms[i]["@id"] === id) {
        return terms[i];
      }
    }
  },
  getTermId: state => (voc, label) => {
    var terms = state.vocabularies[voc].terms;

    for (var i = 0; i < terms.length; i++) {
      if (terms[i]["skos:prefLabel"][0]["@value"] === label) {
        return terms[i]["skos:exactMatch"][0];
      }
    }
  },
  getSelectionItems: state => voc => {
    var terms = state.vocabularies[voc].terms;
    var ar = [];
    for (var i = 0; i < terms.length; i++) {
      ar.push({
        label: terms[i]["skos:prefLabel"][0]["@value"],
        id: terms[i]["skos:exactMatch"][0]
      });
    }
    return ar;
  },
  getTermProperty: state => (voc, id, prop) => {
    var terms = state.vocabularies[voc].terms;
    for (var i = 0; i < terms.length; i++) {
      if (terms[i]["@id"] === id) {
        return terms[i][prop];
      }
    }
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
