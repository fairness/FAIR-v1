const state = {
  contributors: 1,
  projects: 1,
  containerTitle: "Container Title",
  filesTitle: [],
  human: false,
  anatomicalCategory: "",
  life: [],
  biologicalMacromolecule: [],
  assayTarget: [],
  bioassayType: "",
  assayOrganism: [],
  organismAssayTarget: "",
  assayDetection: []
};

const mutations = {
  changeContrib(state, contributors) {
    state.contributors = contributors;
  },
  changeProj(state, projects) {
    state.projects = projects;
  },
  changeContainer(state, containerTitle) {
    state.containerTitle = containerTitle;
  },
  setFiles(state, filesTitle) {
    state.filesTitle = filesTitle;
  },
  removeFiles(state) {
    state.filesTitle = [];
  },
  isHuman(state, human) {
    state.human = human;
  },
  anatomical_category(state, anatomicalCategory) {
    state.anatomicalCategory = anatomicalCategory;
  },
  life(state, life) {
    state.life = life;
  },
  biological_macromolecule(state, biologicalMacromolecule) {
    state.biologicalMacromolecule = biologicalMacromolecule;
  },
  assay_target(state, assayTarget) {
    state.assayTarget = assayTarget;
  },
  bioassay_type(state, bioassayType) {
    state.bioassayType = bioassayType;
  },
  assay_organism(state, assayOrganism) {
    state.assayOrganism = assayOrganism;
  },
  organism_assay_target(state, organismAssayTarget) {
    state.organismAssayTarget = organismAssayTarget;
  },
  assay_detection(state, assayDetection) {
    state.assayDetection = assayDetection;
  }
};

/*const getters = {
  contributors: state => state.contributors,
  projects: state => state.projects,
  containerTitle: state => state.containerTitle,
  filesTitle: state => state.filesTitle,
  human: state => state.human,
  anatomicalCategory: state => state.anatomicalCategory,
  life: state => state.life,
  biologicalMacromolecule: state => state.biologicalMacromolecule,
  assayTarget: state => state.assayTarget
};*/

export default {
  state,
  mutations
};
