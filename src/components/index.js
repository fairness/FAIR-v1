import PImg from "./utils/PImg";
import PDDimension from "./display/phaidra_display/PDDimension.vue";
import PDDuration from "./display/phaidra_display/PDDuration.vue";
import PDEntity from "./display/phaidra_display/PDEntity.vue";
import PDExactMatch from "./display/phaidra_display/PDExactMatch.vue";
import PDFunder from "./display/phaidra_display/PDFunder.vue";
import PDGeoreference from "./display/phaidra_display/PDGeoreference.vue";
import PDJsonld from "./display/pharma_display/PDJsonld.vue";
import PhDJsonld from "./display/phaidra_display/PhDJsonld.vue";
import PDJsonldLayout from "./display/phaidra_display/PDJsonldLayout.vue";
import PhDCellLineName from "./display/phaidra_display/PhDCellLineName.vue";
import PhDJsonldLayout from "./display/pharma_display/PhDJsonldLayout.vue";
import PDLangValue from "./display/phaidra_display/PDLangValue.vue";
import PDLicense from "./display/phaidra_display/PDLicense.vue";
import PDProject from "./display/phaidra_display/PDProject.vue";
import PDSeries from "./display/phaidra_display/PDSeries.vue";
import PDBfPublication from "./display/phaidra_display/PDBfPublication.vue";
import PDCitation from "./display/phaidra_display/PDCitation.vue";
import PDAdaptation from "./display/phaidra_display/PDAdaptation.vue";
import PDSkosPreflabel from "./display/phaidra_display/PDSkosPreflabel.vue";
import PDKeyword from "./display/phaidra_display/PDKeyword.vue";
import PDTitle from "./display/phaidra_display/PDTitle.vue";
import PDLabeledValue from "./display/phaidra_display/PDLabeledValue.vue";
import PDUwmetadata from "./display/phaidra_display/PDUwmetadata.vue";
import PDValue from "./display/phaidra_display/PDValue.vue";
import PIDimension from "./input/phaidra_inputs/PIDimension.vue";
import PIDuration from "./input/phaidra_inputs/PIDuration.vue";
import PIEntity from "./input/phaidra_inputs/PIEntity.vue";
import PIDateEdtf from "./input/phaidra_inputs/PIDateEdtf.vue";
import PIFilenameReadonly from "./input/phaidra_inputs/PIFilenameReadonly.vue";
import PhSubmit from "./input/PhSubmit.vue";
import PIFunder from "./input/phaidra_inputs/PIFunder.vue";
import PIAssociation from "./input/phaidra_inputs/PIAssociation.vue";
import PISeries from "./input/phaidra_inputs/PISeries.vue";
import PIBfPublication from "./input/phaidra_inputs/PIBfPublication.vue";
import PICitation from "./input/phaidra_inputs/PICitation.vue";
import PIAdaptation from "./input/phaidra_inputs/PIAdaptation.vue";
import PISubjectGnd from "./input/phaidra_inputs/PISubjectGnd.vue";
import PISpatialGetty from "./input/phaidra_inputs/PISpatialGetty.vue";
import PISpatialText from "./input/phaidra_inputs/PISpatialText.vue";
import PIProject from "./input/phaidra_inputs/PIProject.vue";
import PISelect from "./input/phaidra_inputs/PISelect.vue";
import PISelectText from "./input/phaidra_inputs/PISelectText.vue";
import PITextField from "./input/phaidra_inputs/PITextField.vue";
import PITextFieldSuggest from "./input/phaidra_inputs/PITextFieldSuggest.vue";
import PITitle from "./input/phaidra_inputs/PITitle.vue";
import PIFile from "./input/phaidra_inputs/PIFile";
import PIVocabExtReadonly from "./input/phaidra_inputs/PIVocabExtReadonly.vue";
import PISpatialGettyReadonly from "./input/phaidra_inputs/PISpatialGettyReadonly";
import PILiteral from "./input/phaidra_inputs/PILiteral";
import PIKeyword from "./input/phaidra_inputs/PIKeyword";
import PSearch from "./search/PSearch";
import PMDelete from "./management/PMDelete";
import PMSort from "./management/PMSort";
import PMRights from "./management/PMRights";
import PMRelationships from "./management/PMRelationships";
import PMFiles from "./management/PMFiles";

const Components = {
  PImg,
  PDDuration,
  PDDimension,
  PDEntity,
  PDExactMatch,
  PDFunder,
  PDGeoreference,
  PDJsonld,
  PhDJsonld,
  PDJsonldLayout,
  PhDJsonldLayout,
  PhDCellLineName,
  PDLangValue,
  PDLicense,
  PDSeries,
  PDBfPublication,
  PDCitation,
  PDAdaptation,
  PDProject,
  PDSkosPreflabel,
  PDKeyword,
  PDTitle,
  PDLabeledValue,
  PDUwmetadata,
  PDValue,
  PIDimension,
  PIEntity,
  PIDateEdtf,
  PIFilenameReadonly,
  PIFile,
  PhSubmit,
  PIFunder,
  PIAssociation,
  PISeries,
  PIBfPublication,
  PICitation,
  PIAdaptation,
  PISubjectGnd,
  PISpatialGetty,
  PISpatialText,
  PIProject,
  PISelect,
  PISelectText,
  PITextField,
  PITextFieldSuggest,
  PITitle,
  PIVocabExtReadonly,
  PISpatialGettyReadonly,
  PILiteral,
  PIKeyword,
  PIDuration,
  PSearch,
  PMDelete,
  PMSort,
  PMRights,
  PMRelationships,
  PMFiles
};

export default {
  install(Vue) {
    Object.keys(Components).forEach(name => {
      Vue.component(name, Components[name]);
    });
  },
  ...Components
};