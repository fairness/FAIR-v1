import fields from "../utils/fields";

export const matchsubject = {
  methods: {
    getCategory: function(type) {
      if (fields.getFieldbySubject(type)) {
        if (fields.getFieldbySubject(type)["category"]) {
          return fields.getFieldbySubject(type)["category"][0];
        }
      }
    },

    getDescription: function(type) {
      if (fields.getFieldbySubject(type)) {
        if (fields.getFieldbySubject(type)["description"]) {
          return fields.getFieldbySubject(type)["description"];
        }
      }
    },
    getLabel: function(type) {
      if (fields.getFieldbySubject(type)) {
        if (fields.getFieldbySubject(type)["label"]) {
          return fields.getFieldbySubject(type)["label"];
        }
      }
    },
    getinfoRef: function(type) {
      if (fields.getFieldbySubject(type)) {
        if (fields.getFieldbySubject(type)["infoRef"]) {
          return fields.getFieldbySubject(type)["infoRef"];
        }
      }
    }
  }
};
