export const parseoutput = {
  methods: {
    getSkosprefLabel: function(jsonObject) {
      if (jsonObject) {
        if (jsonObject["skos:prefLabel"]) {
          var labelArray = jsonObject["skos:prefLabel"];
          if (labelArray[0]) {
            if (labelArray[0]["@value"]) {
              return labelArray[0]["@value"];
            }
          }
        }
      }
    },

    getSkosexactMatch: function(jsonObject) {
      if (jsonObject) {
        if (jsonObject["skos:exactMatch"]) {
          var labelArray = jsonObject["skos:exactMatch"];
          if (labelArray[0]) {
            return labelArray[0];
          }
        }
      }
    },

    getAccessionnumber: function(jsonObject) {
      if (jsonObject) {
        if (jsonObject["accessionNumber"]) {
          return jsonObject["accessionNumber"];
        }
      }
    },
    getCelllinename: function(jsonObject) {
      if (jsonObject) {
        if (jsonObject["cellLineName"]) {
          var labelArray = jsonObject["cellLineName"];
          return labelArray;
        }
      }
    },
    getUniprotName: function(jsonObject) {
      if (jsonObject) {
        if (jsonObject["uniprotName"]) {
          return jsonObject["uniprotName"];
        }
      }
    },
    getProteinRecommendedName: function(jsonObject) {
      if (jsonObject) {
        if (jsonObject["proteinRecommendedName"]) {
          return jsonObject["proteinRecommendedName"];
        }
      }
    },
    getUniprotAccession: function(jsonObject) {
      if (jsonObject) {
        if (jsonObject["uniprotAccession"]) {
          return jsonObject["uniprotAccession"][0];
        }
      }
    },
    getOrganismName: function(jsonObject) {
      if (jsonObject) {
        if (jsonObject["organismName"]) {
          return jsonObject["organismName"];
        }
      }
    },
    getProteinAlternativeName: function(jsonObject) {
      if (jsonObject) {
        if (jsonObject["proteinAlternativeName"]) {
          return jsonObject["proteinAlternativeName"];
        }
      }
    },
    getSynonyms: function(jsonObject) {
      if (jsonObject) {
        if (jsonObject["skos:altLabel"]) {
          if (Array.isArray(jsonObject["skos:altLabel"])) {
            if (jsonObject["skos:altLabel"][0]) {
              if (jsonObject["skos:altLabel"][0]["@value"]) {
                return jsonObject["skos:altLabel"][0]["@value"];
              } else {
                return jsonObject["skos:altLabel"][0];
              }
            } else {
              return jsonObject["skos:altLabel"][0];
            }
          } else {
            return jsonObject["skos:altLabel"];
          }
        }
      }
    }
  }
};
