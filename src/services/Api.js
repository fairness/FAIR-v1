import axios from "axios";

export default () => {
  return axios.create({
    //baseURL: this.$store.state.settings.instance.mongo
    baseURL: "http://localhost:8081"
  });
};
