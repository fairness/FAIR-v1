import Api from "@/services/Api";

export default {
  fetchPosts() {
    console.log("in fetch posts", Api().get("posts"));
    return Api().get("posts");
  },

  addPost(params) {
    return Api().post("posts", params);
  }
};
