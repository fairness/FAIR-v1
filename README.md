# FAIR-v1



```

Issue

There are still some bugs in diplay. Bioassay not fully working.

TO DOs: Edit, templates, search 

```


# dev

## Prerequisities

You might need more watchers, see [Webpack documentation](https://webpack.js.org/configuration/watch/#not-enough-watchers)

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
